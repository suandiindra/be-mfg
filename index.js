const express = require("express");
const cors = require("cors");
const http = require("http");
const app = express();
const server = http.createServer(app);
const bodyParser = require("body-parser");
// const userRoute = require("./routes/userRoute.js");
const AuthRoute = require("./routes/AuthRoute")
const FormulaRoute = require("./routes/FormulaRoute.js");
const PartnerRoute = require("./routes/PartnerRoute.js");
const ProductRoute = require("./routes/ProductRoute");
const TransaksiMasukRoute = require("./routes/TransaksiMasukRoute.js");
const ItemRoute = require("./routes/ItemRoute.js");
const SpkRoute = require("./routes/SpkRoute.js");
const ReportReoute = require("./routes/ReportReoute.js");

const PORT = process.env.PORT || 800;
app.use(express.json());
app.use(cors());
// const corsOptions = {
//   // origin: 'https://mfg.spinterindo.com', // Atur domain yang diizinkan
//   // origin: 'http://localhost:3000',
//   origin: '*',
//   methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
//   credentials: true, // Aktifkan kredensial
//   optionsSuccessStatus: 204,
// };

// app.use(cors())
// app.use(cors(corsOptions));
app.use(
  bodyParser.urlencoded({
    extended: false,
  })
);

// app.use("/user",userRoute);
app.use("/item", ItemRoute);
app.use("/product", ProductRoute);
app.use("/auth", AuthRoute);
app.use("/formula", FormulaRoute);
app.use("/partner", PartnerRoute);
app.use("/transaksimasuk", TransaksiMasukRoute);
app.use("/spk", SpkRoute);
app.use("/report", ReportReoute);

server.listen(PORT, () => {
  console.log(`The Service running well...in http://localhost:${PORT}`);
});