const mysql = require("mysql2/promise");
const uuidv4 = require('uuid');
const { DB } = require('../config/conf.js');

const createKategori = async (req, res) => {
    const { kategori } = req.body;
    console.log(req.body);
    const request = DB.promise();
    try {
        const insertQuery = `INSERT INTO m_kategori (m_kategori_id,kategori) VALUES (uuid(),'${kategori}')`;
        await request.query(insertQuery);
        return res.status(200).json({ msg: 'Kategori Created Successfully' });
    } catch (error) {
        console.log(error);
        res.status(500).json({ msg: error.message });
    }
};
const delKategori = async (req, res) => {
    const { id } = req.body;
    console.log(req.body);
    const request = DB.promise();
    try {
        const insertQuery = `update m_kategori set isactive = 0 where m_kategori_id = '${id}'`;
        console.log(insertQuery);
        await request.query(insertQuery);
        return res.status(200).json({ msg: 'Kategori Created Successfully' });
    } catch (error) {
        console.log(error);
        res.status(500).json({ msg: error.message });
    }
};
// getKategori function
const getKategori = async (req, res) => {
    try {
        const request = DB.promise();
        const selectQuery = 'SELECT * FROM m_kategori WHERE isactive = 1';
        console.log(selectQuery);
        let results = await request.query(selectQuery);
        console.log(results[0]);
        return res.status(200).json(results[0]);
    } catch (error) {
        console.log(error);
        res.status(500).json({ msg: error.message });
    }
};

module.exports = {
    createKategori,
    getKategori,delKategori
};