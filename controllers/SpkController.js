const express = require('express')
const { DB } = require('../config/conf.js');
const { v4: uuidv4 } = require('uuid');

const pdfs = require("html-pdf");
const { setTemplatePenawaran } = require("./CetakSPK.js");
const { setTemplatePermintaan } = require("./CetakSPKRequest.js");
const fs = require('fs');
// const axios = require('axios')
// const axios = require('axios')

const getProdukFormula = async (req, res) => {
    try {
        // console.log('aaa');
        const { nama_barang, qty, berat, adukan } = req.body
        const request = DB.promise();
        let sel = `select nama_barang,kode_barang 
            ,m_formula_id,formula_extruder,formula_laminating 
            ,formula_printing ,formula_packaging
            ,is_wip_mixer,is_wip_extruder,is_wip_printing,is_wip_packing,is_wip_laminating
            from m_product mp where nama_barang = '${nama_barang}' and mp.isactive = 1`
        console.log(sel);
        let dp = await request.query(sel)
        let hasil = dp[0][0]
        let filterx = `'${hasil.m_formula_id}','${hasil.formula_extruder}'
                        ,'${hasil.formula_laminating}','${hasil.formula_printing}','${hasil.formula_packaging}'`

        console.log(filterx);

        // return
        let selFormula = `select fd.*,b.keterangan as sections
                ,b.kode_formula,b.nama_formula 
                ,c.m_section_id ,c.kode_section
                from formula_detail fd 
                inner join formula b on b.formula_id = fd.formula_id 
                inner join m_section c on c.nama_section = b.keterangan
                where fd.formula_id in (${filterx})`

        console.log(selFormula);
        let data = await request.query(selFormula)
        console.log(data[0]);
        res.status(200).json(data[0]);

    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
}
function nodoc(v) {
    const today = new Date();
    const year = today.getFullYear().toString().substr(-2); // Ambil dua digit terakhir dari tahun
    const month = (today.getMonth() + 1).toString().padStart(2, '0');
    const urutan = v;
    const nomorDokumen = `SPK/${year}/${month}/${urutan.toString().padStart(4, '0')}`;

    return nomorDokumen;
}
const createSPK = async (req, res) => {
    try {
        const { tglSPK, barang, qty, nomorPO, qtyDus, qtyPerDus, partner, warna,
            lineExt, linePrint, lineLam, linePak, listNat,
            panjang, tebal, berat, lebar, lineMixer, dtlFormula, username } = req.body
        const request = DB.promise();

        console.log(warna);
        // console.log(dtlFormula);
        // return
        let cekdulu = `select * from spk where isactive = 1 and trim(nomor_po) = '${nomorPO.trim()}' 
        and trim(nama_product) = '${barang.trim()}'`
        let [ops] = await request.query(cekdulu);
        if (ops.length > 0) {
            return res.status(200).json({ kode: 500, msg: `Nomor PO dan Nama Produk sudah pernah dibuat` });
        }


        let urut = `select count(*) + 1 as urut,uuid() as kys from spk where date_format(createddate,'%Y-%m') = date_format(now(),'%Y-%m')`
        let op = await request.query(urut);
        let urutan = op[0][0].urut
        let keys = op[0][0].kys
        let nodok = nodoc(urutan)
        let warna_mix = ``
        let cp = dtlFormula.filter((o) => { return o.sections == 'MIXER' })
        // console.log(cp);
        if (cp.length > 0) {
            let formula = cp[0].formula_id
            let scl = `select * from formula where formula_id = '${formula}'`
            let dp = await request.query(scl)
            dp = dp[0]
            warna_mix = dp[0].warna
        }

        let insert = `INSERT INTO spk
        (spk_id, nomor_spk, createddate, partner_id, nomor_po,tipe, panjang
        , lebar, tebal, berat, qty_kemasan, warna_mixing, line_mixing, line_extruder, line_printing, 
        line_laminating, line_packaging, isactive, kode_status, status_spk,qty_produksi,nama_product,listnat,created_by)
        VALUES('${keys}', '${nodok}', now(), '${partner}', '${nomorPO}', NULL, ${panjang}
        , ${lebar}, ${tebal}, ${berat}, '${qtyDus}','${warna}', '${lineMixer}', 
        '${lineExt}', '${linePrint}', '${lineLam ? lineLam : ''}', '${linePak}', 1, 'WT1', 'WAITING',${qty},'${barang}','${listNat}','${username}')`

        // console.log(insert);
        // return
        let awt = await request.query(insert)
        console.log(insert);
        for (let i = 0; i < dtlFormula.length; i++) {
            let obj = dtlFormula[i]
            let inse = `INSERT INTO spk_detail_formula
            (spk_detail_formula_id, spk_id, formula_id, kode_formula
            , nama_formula, kode_item, nama_item, satuan
            , qty, amount, sections, kode_section, createddate)
            VALUES(uuid(),'${keys}','${obj.formula_id}', '${obj.kode_formula}'
            ,'${obj.kode_formula}','${obj.kode_item}','${obj.nama_item}','${obj.satuan}'
            ,'${obj.qty}', ${obj.amount},'${obj.sections}','${obj.kode_section}',now())`
            await request.query(inse)

            let updStok = `update m_item set qty = qty - ${obj.qty} 
            ,updatedAt = now()
            where kode_item = '${obj.kode_item}'`

            await request.query(updStok)
        }
        return res.status(200).json({ kode: 200, msg: `Berhasil` });
    } catch (error) {
        console.log(error);
        return res.status(500).json(error);

    }
}
const findOne = async (req, res) => {
    try {
        const { spk_id } = req.query
        const request = DB.promise();
        let sel = `select 
        s.spk_id 
        ,s.nomor_spk
        ,date_format(s.createddate ,'%Y-%m-%d') as tgl,nomor_po
        ,c.nama_partner 
        ,nama_product 
        ,max(qty_produksi) as qty 
        ,max(berat) as berat
        ,max(s.panjang) as panjang 
        ,max(coalesce(s.lebar,0)) as lebar
        ,sum(b.qty) as qty_adukan
        ,max(s.line_mixing) as line_mixing 
        ,max(s.line_extruder) as line_extruder 
        ,max(s.line_printing) as line_printing 
        ,max(s.line_laminating) as line_laminating 
        ,max(s.line_packaging) as line_packaging
        ,kode_status ,status_spk 
        ,concat('https://be.spinterindo.com/product/getImage/',d.img) as img
        ,warna_mixing,
        note_mixer,note_extruder,note_printing,note_laminating,note_packing
        ,listnat
        from spk s 
        inner join spk_detail_formula b on b.spk_id = b.spk_id and b.qty > 0 
        inner join m_partner c on c.m_partner_id = s.partner_id 
        inner join m_product d on d.nama_barang = s.nama_product 
        where s.isactive = 1 and s.spk_id = '${spk_id}'
        group by s.spk_id,s.nomor_spk,date_format(s.createddate ,'%Y-%m-%d') ,nomor_po
        ,kode_status ,status_spk,warna_mixing,listnat `

        // console.log(sel);
        // return
        let slc = await request.query(sel)

        let selFormula = `select kode_formula,formula_id ,sdf.sections ,sdf.kode_section,b.m_section_id 
        from spk_detail_formula sdf 
        inner join m_section b on b.kode_section = sdf.kode_section  and length(formula_id) > 12
        where spk_id = '${spk_id}' and sdf.qty > 0
        group by kode_formula,formula_id ,sections ,kode_section order by m_section_id asc`

        let dp = await request.query(selFormula)
        let obj = dp[0]
        for (x = 0; x < obj.length; x++) {
            let cl = `select * from spk_detail_formula 
            where kode_formula = '${obj[x].kode_formula}' and spk_id = '${spk_id}' 
            and sections = '${obj[x].sections}' and qty > 0`
            console.log(cl);
            let wp = await request.query(cl)
            obj[x] = { ...obj[x], dtl: wp[0] }
        }

        slc = slc[0][0]
        slc = { ...slc, formula: obj }
        return res.status(200).json(slc);
    } catch (error) {
        console.log(error);
        return res.status(500).json(error);
    }
}
const addnote = async (req, res) => {
    try {
        const { spk_id, sec, note } = req.body
        const request = DB.promise();
        let updt = ``
        if (sec == "MIXER") {
            updt = ` note_mixer = '${note}' `
        } else if (sec == "EXTRUDER") {
            updt = ` note_extruder = '${note}' `
        } else if (sec == "PRINTING") {
            updt = ` note_printing = '${note}' `
        } else if (sec == "LAMINATING") {
            updt = ` note_laminating = '${note}' `
        } else if (sec == "PACKING") {
            updt = ` note_packing = '${note}' `
        }
        let up = `update spk set ${updt} where spk_id = '${spk_id}'`
        let rp = await request.query(up)
        if (rp) {
            return res.status(200).json({ msg: `Berhasil` });
        }
    } catch (error) {
        console.log(error);
        return res.status(500).json(error);
    }
}
const find = async (req, res) => {
    try {
        const { tgl1, tgl2, kode_status } = req.query
        const request = DB.promise();
        let filter = ``
        if (tgl2) {
            filter = ` and date_format(s.createddate ,'%Y-%m-%d') between '${tgl1}' and '${tgl2}' `
        } else {
            filter = ` and date_format(s.createddate ,'%Y-%m-%d') = date_format(now() ,'%Y-%m-%d') `
        }

        if (kode_status) {
            filter = filter + ` and kode_status = '${kode_status}' `
        }
        let sel = `select 
        s.spk_id 
        ,s.nomor_spk
        ,date_format(s.createddate ,'%Y-%m-%d') as tgl,nomor_po
        ,c.nama_partner 
        ,nama_product 
        ,max(qty_produksi) as qty 
        ,max(berat) as berat
        ,max(panjang) as panjang 
        ,max(coalesce(lebar,0)) as lebar
        ,kode_status ,status_spk ,s.created_by
        from spk s 
        -- inner join spk_detail_formula b on b.spk_id = b.spk_id 
        inner join m_partner c on c.m_partner_id = s.partner_id 
        where s.isactive = 1
        ${filter}
        group by s.spk_id,s.nomor_spk,date_format(s.createddate ,'%Y-%m-%d') ,nomor_po
        ,kode_status ,status_spk 
        order by s.createddate desc`

        console.log(sel);

        let slc = await request.query(sel)
        slc = slc[0]
        res.status(200).json(slc);

    } catch (error) {
        res.status(500).json({ msg: error });
    }
}
const cetak = async (req, res) => {
    try {
        const { spk_id, section } = req.query
        const request = DB.promise();
        let sel1 = `select 
        s.spk_id 
        ,s.nomor_spk 
        ,date_format(s.createddate ,'%Y-%m-%d') as tgl,nomor_po
        ,c.nama_partner 
        ,nama_product 
        ,max(qty_produksi) as qty 
        ,max(berat) as berat
        ,max(d.tebal) as tebal
        ,max(s.panjang) as panjang 
        ,max(d.jenis_barang ) as tipe
        ,max(s.qty_kemasan) as qty_kemasan
        ,max(s.warna_mixing) as warna_mixing
        ,max(s.line_mixing) as line_mixing 
        ,max(s.line_extruder) as line_extruder 
        ,max(s.line_printing) as line_printing 
        ,max(s.line_laminating) as line_laminating 
        ,max(s.line_packaging) as line_packaging
        ,max(coalesce(s.lebar,0)) as lebar
        ,sum(b.qty) as qty_adukan
        ,kode_status ,status_spk 
        ,concat('https://be.spinterindo.com/product/getImage/',d.img) as img
        ,d.img as img_fg
        ,e.warna
        ,s.note_mixer ,s.note_extruder ,s.note_printing ,s.note_laminating ,s.note_packing 
        ,listnat
        from spk s 
        inner join spk_detail_formula b on b.spk_id = b.spk_id 
        inner join m_partner c on c.m_partner_id = s.partner_id 
        inner join m_product d on d.nama_barang = s.nama_product and d.isactive  = 1
        inner join formula e on e.formula_id = b.formula_id and e.keterangan = 'MIXER'
        where s.isactive = 1 and s.spk_id = '${spk_id}'
        group by s.spk_id,date_format(s.createddate ,'%Y-%m-%d') ,nomor_po
        ,kode_status ,status_spk
        ,s.note_mixer ,s.note_extruder ,s.note_printing ,s.note_laminating ,s.note_packing,s.listnat `

        let sp = await request.query(sel1)
        sp = sp[0]
        //section
        let qty_adukan = 0
        if (section == "MIXING") {
            let sk = `select sum(qty) as qty 
            from spk_detail_formula sdf where spk_id = '${spk_id}'
            and sections = '${section}'`
            let spd = await request.query(sk)
            spd = spd[0][0]
            qty_adukan = spd.qty
        }
        console.log(sp[0]);
        let html = await setTemplatePenawaran(sp[0], section, qty_adukan);
        const options = {
            format: "Letter", timeout: '100000'

        };
        // Buat file PDF
        pdfs
            .create(html, options)
            .toFile(`assets/SPK/${section}-${spk_id}.pdf`, (err, resx) => {
                if (err) return console.error(err);
                console.log("File PDF berhasil dibuat:", resx.filename);
                res.sendFile(resx.filename);
            });

    } catch (error) {
        console.log(error);
        res.status(500).json({ msg: error });
    }
}
async function convertImageToBase64(url) {
    try {
        // const response = await axios.get(url, { responseType: 'arraybuffer' });
        // const base64Data = Buffer.from(response.data, 'binary').toString('base64');
        const imageBuffer = fs.readFileSync(`./assets/interindo.png`);
        const base64Data = imageBuffer.toString('base64');
        return base64Data;
    } catch (error) {
        console.error('Error fetching image:', error.message);
        throw error;
    }
}
const testCetak = async (req, res) => {
    try {
        const { spk_id, section } = req.query
        const request = DB.promise();
        let dp = ''
        dp = await convertImageToBase64('https://be.spinterindo.com/product/logoImg')
        // fs.readFileSync('http://localhost:800/product/logoImg').toString('base64');
        // console.log(dp);
        let html = `<!DOCTYPE html>
        <html lang="en">
        <body>
            <p></p>
            <img src="data:image/png;base64,${dp}" alt="Red dot" />
        </body>
        </html>`

        const options = {
            format: "Letter",
        };
        pdfs
            .create(html, options)
            .toFile(`assets/SPK/HASILTEST.pdf`, (err, resx) => {
                if (err) return console.error(err);
                console.log("File PDF berhasil dibuat:", resx.filename);
                res.sendFile(resx.filename);
            });

    } catch (error) {
        console.log(error);
        res.status(500).json({ msg: error });

    }
}
const cetkPermintaan = async (req, res) => {
    try {
        const { spk_id, sec } = req.query
        const request = DB.promise();
        let sel = `select s.nomor_po,nomor_spk,s.nama_product,c.nama_partner,
        s.line_mixing ,s.line_extruder ,s.line_printing ,s.line_laminating 
        ,s.line_packaging 
        ,b.* from spk s
        inner join spk_detail_formula b on b.spk_id = b.spk_id 
        inner join m_partner c on c.m_partner_id = s.partner_id 
        where s.spk_id = '${spk_id}'
        order by b.kode_section `
        // console.log(sel);
        let sp = await request.query(sel)
        sp = sp[0]

        let html = setTemplatePermintaan(sp, sec);
        const options = {
            format: "Letter",
        };
        // Buat file PDF
        pdfs
            .create(html, options)
            .toFile(`assets/SPK/REQ${sec}-${spk_id}.pdf`, (err, resx) => {
                if (err) return console.error(err);
                console.log("File PDF berhasil dibuat:", resx.filename);
                res.sendFile(resx.filename);
            });
        // res.status(200).json({data:html});

    } catch (error) {
        console.log(error);
        res.status(500).json({ msg: error });
    }
}
const findFormulaSec = async (req, res) => {
    try {
        const { nomorspk, section } = req.query
        const request = DB.promise();

        let sel = `select s.nomor_spk, s.nama_product,s.line_mixing,line_extruder,line_printing,line_laminating ,line_packaging,nomor_spk,nomor_po ,b.* from spk s 
        inner join spk_detail_formula b on b.spk_id = s.spk_id 
        where s.nomor_spk = '${nomorspk}' and b.sections = '${section}'`

        let rd = await request.query(sel)
        return res.status(200).json(rd[0]);
    } catch (error) {
        console.log(error);
        res.status(500).json({ msg: error });
    }
}

const deleteResult = async (req, res) => {
    console.log('x');
    try {
        const { spk_result_id } = req.body
        const request = DB.promise();
        let dels = `update spk_result set isactive = 0 where spk_result_id = '${spk_result_id}'`
        console.log(dels);
        let rp = await request.query(dels)
        if (rp) {
            return res.status(200).json({ msg: `Berhasil` });
        }
    } catch (error) {
        console.log(error);
        return res.status(500).json({ msg: error });
    }
}
const hasil = async (req, res) => {
    try {
        const { objek } = req.body
        const request = DB.promise();
        let ins = ``
        if (objek.section == 'MIXER') {
            ins = `INSERT INTO spk_result
            (spk_result_id, spk_id, sections, createdate, periode,shift
            , jam_mulai, jam_selesai, aditif, operator, koordinator
            ,adukan, sisa, reject, qty, berat, cycletime, rpm, isactive)
            VALUES(uuid(),'${objek.key_id}', '${objek.section}', now(),'${objek.tgl}','${objek.shift}'
            ,'${objek.start}', '${objek.end}', '${objek.aditif}',null,'${objek.koordinator}'
            ,'${objek.adukan}','${objek.sisa}', NULL,'${objek.adukan}', NULL, NULL, NULL,1)`
        } else if (objek.section == 'EXTRUDER' || objek.section == 'LAMINATING') {
            // console.log(objek.key_id,objek.reject,objek.berat);
            let rejectPersen = await hitungReject(objek.key_id, objek.reject)
            ins = `INSERT INTO spk_result
            (spk_result_id, spk_id, sections, createdate, periode,shift
            , jam_mulai, jam_selesai, aditif, operator, koordinator
            ,adukan, sisa, reject, qty, berat, cycletime, rpm, isactive,persen)
            VALUES(uuid(),'${objek.key_id}', '${objek.section}', now(),'${objek.tgl}','${objek.shift}'
            ,'${objek.start}', '${objek.end}', null,'${objek.operator}','${objek.koordinator}'
            ,null,null, '${objek.reject}','${objek.qty}', '${objek.berat}', '${objek.cycle}'
            , '${objek.rpm}',1,'${rejectPersen}')`

        } else if (objek.section == 'PRINTING') {
            let rejectPersen = await hitungReject(objek.key_id, objek.reject, 1)
            ins = `INSERT INTO spk_result
            (spk_result_id, spk_id, sections, createdate, periode,shift
            , jam_mulai, jam_selesai, aditif, operator, koordinator
            ,adukan, sisa, reject, qty, berat, cycletime, rpm, isactive,persen)
            VALUES(uuid(),'${objek.key_id}', '${objek.section}', now(),'${objek.tgl}',null
            ,'${objek.start}', null, null,'${objek.operator}','${objek.koordinator}'
            ,null,null, '${objek.reject}','${objek.qty}', '${objek.qtydus}',null, null,1,'${rejectPersen}')`
        } else if (objek.section == 'PACKING') {
            let rejectPersen = await hitungReject(objek.key_id, objek.reject, 1)

            ins = `INSERT INTO spk_result
            (spk_result_id, spk_id, sections, createdate, periode,shift
            , jam_mulai, jam_selesai, aditif, operator, koordinator
            ,adukan, sisa, reject, qty, berat, cycletime, rpm, isactive,persen,qty_dus)
            VALUES(uuid(),'${objek.key_id}', '${objek.section}', now(),'${objek.tgl}',null
            ,'${objek.start}', null, null,'${objek.operator}','${objek.koordinator}'
            ,null,null,'${objek.reject}','${objek.qty}',null,null, null,1,'${rejectPersen}',${objek.qty_dus ? objek.qty_dus : null})`

            console.log(ins);
        }

        let rp = await request.query(ins)
        if (rp) {
            if (objek.section == 'PACKING') {
                console.log('asss');
                let sel = `select b.* from spk a
                inner join m_product b on b.nama_barang = a.nama_product 
                where a.spk_id = '${objek.key_id}'`

                let rp = await request.query(sel)
                let m_product_id = rp[0][0].m_product_id
                let kode = rp[0][0].kode_barang
                let nama_barang = rp[0][0].nama_barang

                let sel1 = `select * from stok_fg where m_product_id = '${m_product_id}' 
                and date_format(period,'%Y-%m') = date_format(now(),'%Y-%m')`

                console.log(sel1);
                let dt = await request.query(sel1)
                if (dt[0].length > 0) {
                    let upd = `update stok_fg set stok_masuk = stok_masuk + ${objek.qty} 
                    ,stok_akhir = stok_akhir + ${objek.qty} 
                    ,lastupdate = now()
                    where m_product_id = '${m_product_id}' 
                    and date_format(period,'%Y-%m') = date_format(now(),'%Y-%m')`
                    let dt = await request.query(upd)
                } else {
                    let masukStok = `INSERT INTO stok_fg
                    (stok_rm_id, period, m_product_id, kode_barang, nama_barang, stok_awal, stok_masuk, stok_keluar, stok_adjust, stok_akhir, lastupdate)
                    VALUES(uuid(), now(), '${m_product_id}','${kode}','${nama_barang}',null, ${objek.qty} , NULL, NULL, ${objek.qty} , now());`

                    console.log(masukStok);
                    await request.query(masukStok)
                }

                let spk = `select * from spk where spk_id = '${objek.key_id}'`
                console.log(spk);
                let hsl = await request.query(spk)
                hsl = hsl[0][0]
                let order = hsl.qty_produksi

                let px = `select sum(qty) as qty from spk_result where sections = 'PACKING'
                and spk_id = '${objek.key_id}'`
                let hsl2 = await request.query(px)
                hsl2 = hsl2[0][0]
                let order2 = hsl2.qty

                let status = 'On Progress'
                let kodes = 'WT2'
                if (order2 >= order) {
                    status = 'Complete'
                    kodes = 'WT3'
                }

                let updh = `update spk set status_spk = '${status}',kode_status = '${kodes}' where spk_id = '${objek.key_id}'`
                await request.query(updh)
            }
        }

        return res.status(200).json({ msg: `berhasil` });
    } catch (error) {
        console.log(error);
        res.status(500).json({ msg: error });
    }
}
const deleteSPK = async (req, res) => {
    try {
        const { spk_id } = req.body
        const request = DB.promise();
        let sel = `select * from spk_result where isactive = 1 and spk_id = '${spk_id}'`
        let [dp] = await request.query(sel);
        if (dp.length > 0) {
            return res.status(200).json({ kode: 500, msg: `Sudah Ada hasil SPK` });
        } else {
            let del = `update spk set isactive = 0 where spk_id = '${spk_id}'`
            let px = await request.query(del);
            if (px) {
                return res.status(200).json({ kode: 200, msg: `Berhasil Hapus SPK` });
            }
        }
    } catch (error) {
        console.log(error);
        res.status(500).json({ msg: error });
    }
}
const hitungReject = async (id, rjc, tipe) => {
    const request = DB.promise();
    let selc = `select * from spk where spk_id = '${id}'`
    let [hsl] = await request.query(selc)
    let dt = hsl[0]
    let produksi = dt.qty_produksi
    let berat = dt.berat
    let panjang = dt.panjang

    let hasil = 0
    if (!tipe) {
        hasil = produksi * (berat * (panjang / 100))
    } else {
        hasil = produksi
    }
    let brx = (rjc / hasil) * 100
    return brx
}
function createHTML(data) {
    let htmlContent = `
      <html>
        <head>
          <style>
            body { font-family: Arial, sans-serif; }
            h1 { text-align: center; }
            p { margin: 0; }
          </style>
        </head>
        <body>
          <h1>Profil</h1>
          <p>Nama: ${data.profile.nama}</p>
          <p>Usia: ${data.profile.usia}</p>
          <h1>List</h1>
          <ul>
    `;

    data.list.forEach((item) => {
        htmlContent += `<li>${item}</li>`;
    });

    htmlContent += `
          </ul>
        </body>
      </html>
    `;

    return htmlContent;
}

const testingPdf = async (req, res) => {
    try {
        const data = [
            {
                profile: { nama: "indra", usia: 32 },
                list: ["indra", "irpan", "ilyas"]
            },
            {
                profile: { nama: "irpan", usia: 32 },
                list: ["indra", "irpan", "ilyas"]
            },
            {
                profile: { nama: "ilyas", usia: 12 },
                list: ["indra", "irpan", "ilyas"]
            },
        ];
        data.forEach((item, index) => {
            const html = createHTML(item);
            // pdfs.create(html).toFile(`assets/oke.pdf`, (err, resx) => {
            //     if (err) return console.log(err);
            //     console.log(`berhasil dibuat!`);
            //     res.sendFile(resx.filename);
            // });
            pdfs.create(html).toFile(`assets/oke2.pdf`, (err, resx) => {
                if (err) return console.log(err);
                console.log(`${resx.filename} berhasil dibuat!`);

                // Jika ini adalah file PDF terakhir yang dibuat, print pesan bahwa semua PDF sudah selesai dibuat
                if (index === data.length - 1) {
                    console.log("Semua PDF berhasil dibuat!");
                    res.sendFile(resx.filename);
                }
            });
        });
    } catch (error) {
        console.log(error);
    }
}
module.exports = {
    getProdukFormula, createSPK, find, deleteResult
    , findOne, cetak, cetkPermintaan, findFormulaSec, hasil
    , addnote, deleteSPK, testCetak, testingPdf
}