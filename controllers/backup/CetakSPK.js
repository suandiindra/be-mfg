const path = require("path");
const fs = require("fs");
const {urlGlobal} = require("../config/conf.js")

module.exports = {
    setTemplatePenawaran: function (header,section,adukan) {
        console.log('****',header);
        let html = `<!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Form Survey</title>
            <style>
                .border {
                    display: flex;
                    align-items: center;
                    height: 90px;
                    width: 100%;
                    border-radius: 5px;
                }
                .btm{
                    margin-bottom: -12px;
                    font-family: Tahoma, sans-serif;
                    font-size: 14px;
                }
                .btm2{
                    margin-bottom: -12px;
                    font-family: Tahoma, sans-serif;
                    font-size: 16px;
                }
                .header {
                    font-family: Tahoma, sans-serif;
                    font-weight: bold;
                    font-size: 24px;
                    margin-left: 20px;
                    color: antiquewhite;
                    width: 60%;
                    padding-top: 3px;
                }
                .headerRight {
                    font-family: Tahoma, sans-serif;
                    font-weight: bold;
                    font-size: 24px;
                    margin-left: 20px;
                    width: 40%;
                    padding-top: 3px;
                }
                .infoheader {
                    display: flex;
                    padding: 20px;
                    font-family: Tahoma, sans-serif;
                }
                .infoheader-left {
                    width: 10%;
                    float: left;
                    padding-right: 10px;
                    background-color:'green'
                }
                .kiri {
                    width: 50%;
                    float: left;
                    padding-right: 10px;
                    background-color:'green'
                }
                .kanan {
                    width: 50%;
                    float: right;
                    background-color:'red'
                }
                .infoheader-right {
                    width: 90%;
                    float: right;
                    background-color:'red'
                }
                .infoheader p {
                    font-size: 16px;
                }
                table {
                    width: 100%;
                    border-collapse: collapse;
                    margin-top: 20px;
                    font-family: Tahoma, sans-serif;
                }
                th, td {
                    padding: 10px;
                    text-align: left;
                }
                th {
                    background-color: #f2f2f2; /* Warna latar belakang header */
                }
                tr:nth-child(even) {
                    background-color: #f2f2f2; /* Warna latar belakang bergantian tiap baris genap */
                }
            </style>
        </head>
        <body style="padding:30px">
            <div class="border">
                <div class="header">
                    <img src="${urlGlobal}product/logoImg" style="width:170px;heigh:170px"/>
                </div>
            </div>
            <div style="padding:15px">
                <div class="infoheader-left">
                </div>
                <div class="infoheader-right">
                    <p style="font-weight: bold;font-family: Tahoma, sans-serif;text-align: center;margin-right:20px;margin-top:-100px">WORK IN PROGRESS ${section}</p>
                    <p style="font-weight: bold;font-family: Tahoma, sans-serif;text-align: center;margin-right:20px;margin-top:0px">${header.nomor_spk}</p>
                </div>
            </div>
            <hr style="margin-top:-40px"/>
            <div style="width: 100%;">
                <div style="width: 30%; height: 100px; float: left"> 
                    <img src="${header.img}" style="width:170px;heigh:170px"/>
                </div>
                <div style="width: 70%; height: 100px; float: right">
                    ${sideKonten(header,adukan,section)}
                </div>
            </div>
            <div style="margin-top:350px">
                ${cekView(section)}
            </div>
            <div>
                ${footer()}
            </div>
        </body>
        </html>`
        return html;
    }
}
function mixerResult(){
    let html = `
        <table border="1">
            <tr>
                <td>Shift</td>
                <td>Tanggal</td>
                <td>Jam Mulai</td>
                <td>Jam Selesai</td>
                <td>Co.Aditif</td>
                <td>Koordinator</td>
                <td>Adukan</td>
                <td>Sisa</td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </table>
    `
    return html
}
function cekView(v){
    if(v == "MIXING"){
        return mixerResult()
    }else if(v == "EXTRUDER"){
        return extruderResult()
    }else if(v == "PRINTING"){
        return printingResult()
    }else if(v == "PACKING"){
        return packingResult()
    }
}
function cekLine(v,obj){
    if(v == "MIXING"){
        return obj.line_mixing
    }else if(v == "EXTRUDER"){
        return obj.line_extruder
    }else if(v == "PRINTING"){
        return obj.line_printing
    }else if(v == "LAMINATING"){
        return obj.line_laminating
    }else{
        return ''
    }
}
function extruderResult(){
    let html = `
        <table border="1">
            <tr>
                <td>Shift</td>
                <td>Tanggal</td>
                <td>Jam Mulai</td>
                <td>Jam Selesai</td>
                <td>Operator</td>
                <td>Koordinator</td>
                <td>Qty Lembar</td>
                <td>Reject</td>
                <td>Berat/m1</td>
                <td>Cycle Time</td>
                <td>RPM</td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        `
    return html
}
function printingResult(){
    let html = `
        <table border="1">
            <tr>
                <td>Shift</td>
                <td>Tanggal</td>
                <td>Jam</td>
                <td>Operator</td>
                <td>Koordinator</td>
                <td>Qty Lembar</td>
                <td>Qty Dus</td>
                <td>Ext Lbr</td>
                <td>Print Lbr</td>
            </tr>
            <tr>
                <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
            </tr>
            <tr>
                <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
            </tr>
            <tr>
                <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
            </tr>
            <tr>
                <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
            </tr>
            <tr>
                <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
            </tr>
            <tr>
                <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
            </tr>
            <tr>
                <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
            </tr>
            <tr>
                <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
            </tr>
            <tr>
                <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
            </tr>
            <tr>
                <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
            </tr>
        </table>`
    return html
}
function packingResult(){
    let html = `
        <table border="1">
            <tr>
                <td>Tanggal</td>
                <td>Jam</td>
                <td>Operator</td>
                <td>Koordinator</td>
                <td>Qty</td>
            </tr>
            <tr>
                <td></td><td></td><td></td><td></td><td></td>
            </tr>
            <tr>
                <td></td><td></td><td></td><td></td><td></td>
            </tr>
            <tr>
                <td></td><td></td><td></td><td></td><td></td>
            </tr>
            <tr>
                <td></td><td></td><td></td><td></td><td></td>
            </tr>
            <tr>
                <td></td><td></td><td></td><td></td><td></td>
            </tr>
            <tr>
                <td></td><td></td><td></td><td></td><td></td>
            </tr>
            <tr>
                <td></td><td></td><td></td><td></td><td></td>
            </tr>
            <tr>
                <td></td><td></td><td></td><td></td><td></td>
            </tr>
            <tr>
                <td></td><td></td><td></td><td></td><td></td>        
            </tr>
            <tr>
                <td></td><td></td><td></td><td></td><td></td>
            </tr>
        </table>`
    return html
}
function footer(){
    let html = `
        <div style="width: 100%;">
            <Table border="1">
                <tr style="height:200px">
                    <td>
                        <b>Catatan:<b>
                    </td>
                    <td>
                        <b>Keterangan:<b>
                    </td>
                </tr>
            </table>
            <div>
                <p>Dibuat Oleh : </p>
            </div>
        </div>
    `

    return html
}
function sideKonten(header,adukan,section){
    let html = `<div style="width: 50%; height: 100px; float: left">
        <b>Informasi SPK</b>
        <table>
            <tr>
                <td>Nomor PO</td>
                <td>:</td>
                <td>${header.nomor_po}</td>
            </tr>
            <tr>
                <td>Customer</td>
                <td>:</td>
                <td>${header.nama_partner}</td>
            </tr>
            <tr>
                <td>Kode Index</td>
                <td>:</td>
                <td>${header.tipe}</td>
            </tr>
            <tr>
                <td>Qty Produksi</td>
                <td>:</td>
                <td>${header.qty}</td>
            </tr>
            <tr>
                <td>Qty Dus</td>
                <td>:</td>
                <td>${header.qty_kemasan}</td>
            </tr>
            <tr>
                <td>Qty per Dus</td>
                <td>:</td>
                <td>${parseFloat(header.qty) / parseFloat(header.qty_kemasan)}</td>
            </tr>
        </table>
    </div> 
    <div style="width: 50%; height: 100px; float: right">
        <b>Description Product</b>
        <table>
            <tr>
                <td>Product</td>
                <td>:</td>
                <td>${header.nama_product}</td>
            </tr>
            <tr>
                <td>Panjang</td>
                <td>:</td>
                <td>${header.panjang}</td>
            </tr>
            <tr>
                <td>Lebar</td>
                <td>:</td>
                <td>${header.lebar}</td>
            </tr>
            <tr>
                <td>Tebal</td>
                <td>:</td>
                <td>${header.lebar}</td>
            </tr>
            <tr>
                <td>Warna Papan</td>
                <td>:</td>
                <td>${header.warna}</td>
            </tr>
            <tr>
                <td>Line Mesin</td>
                <td>:</td>
                <td>${cekLine(section,header)}</td>
            </tr>
            ${adukan ? `<tr>
            <td>Jumlah Adukan</td>
            <td>:</td>
            <td>${adukan}</td>
        </tr>` : ""}
        </table>
    </div> `
    return html
}
