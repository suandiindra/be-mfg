const mysql = require("mysql2/promise");
const uuidv4 = require('uuid');
const { DB } = require('../config/conf.js');


const find = async (req, res) => {
    try {
        console.log('woooiiiiii........');
        const request = DB.promise();
        let sel = `select b.email ,a.*,case when a.isVendor = 2 then 'Customer' else 'Vendor' end as partner 
        from m_partner a 
        left join m_user b on b.nama_user =  a.nama_partner 
        where a.isactive = 1`
        let [res1] = await request.query(sel)
        res.status(200).json(res1);
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
}
const reset = async (req, res)=>{
    try {
        const { username,pwd } = req.body;
        const request = DB.promise();
        let sel = `update m_user set password = '${await hashPassword(pwd ? pwd : '12345')}' where email = '${username}'`;
        console.log(sel);

        await request.query(sel);
        res.status(200).json({ msg: `Berhasil Reset` });
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
}
const create = async (req, res) => {
    try {
        const { nama_partner, alamat, isvendor } = req.body;
        const request = DB.promise();

        let px = `select count(*)+1 tx,uuid() as unikId from m_partner where isVendor = 2`
        let [ad] = await request.query(px);
        let angka = ad[0].tx
        let unikId = ad[0].unikId
        let nik = angka.toString().padStart(4, '0');

        let sel = `insert into m_partner (m_partner_id, nama_partner, alamat, isVendor, isactive)
        values ('${unikId}', '${nama_partner}', '${alamat}', '${isvendor ? isvendor : 1}', 1)`;
        await request.query(sel);
        if (isvendor == "2") {
            
            let sel = `INSERT INTO m_user
            (m_user_id, nik, nama_user, email, password, role, createdAt, updatedAt,isactive)
            VALUES('${unikId}', '${nik}', '${nama_partner}', '${nik}', '${await hashPassword('12345')}', '4'
            , now(), now(),1)`

            await request.query(sel)
        }
        res.status(200).json({ msg: `Berhasil Input` });
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
}
async function hashPassword(password) {
    const request = DB.promise();
    let cek = `select SHA2('${password}', 256) as encrypPwd`
    let [dp] = await request.query(cek)
    return dp[0].encrypPwd
}
const update = async (req, res) => {
    try {
        const { nama_partner, alamat, isvendor, isactive, m_partner_id } = req.body;
        const request = DB.promise();
        let sel = `update m_partner set nama_partner = '${nama_partner}', alamat = '${alamat}'
        , isVendor = '${isvendor}', isactive = '${isactive}' where m_partner_id = '${m_partner_id}'`;

        
        await request.query(sel);
        res.status(200).json({ msg: `Berhasil Input` });
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
}

module.exports = { find, create, update,reset };
