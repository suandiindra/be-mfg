const express = require('express')
const { DB } = require('../config/conf.js');
const { v4: uuidv4 } = require('uuid');



const find = async (req, res) => {
    try {
        const request = DB.promise();
        console.log('aaa');
        let sel = `select 
        a.formula_id,a.kode_formula,a.nama_formula,date_format(start_date,'%d %M, %y') as mulai
        ,keterangan,date_format(end_date,'%d %M, %y') as selesai
        ,case when isactive = 1 then 'Ya' else 'Tidak' end as aktivasi,
        ROUND(sum(b.qty),2) as qty_adukan
        from formula a
        inner join formula_detail b on a.formula_id = b.formula_id
        where isactive = 1
        group by a.formula_id,a.kode_formula,keterangan,a.nama_formula,isactive,start_date,end_date
        order by a.createdate desc`

        let [res1] = await request.query(sel)
        res.status(200).json(res1);

    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
}
const updateProductFormula = async (req, res) => {
    try {
        const { mxFormula, m_product_id, extFormula, lmnFormula, pkgFormula, printFormula
            , mixer, extruder, printing, laminating, packing
        } = req.body


        const request = DB.promise();
        let additional = ``
        if (extFormula) {
            additional = additional + `,formula_extruder = '${extFormula}' `
        }
        if (lmnFormula) {
            additional = additional + `,formula_laminating = '${lmnFormula}' `
        }
        if (pkgFormula) {
            additional = additional + `,formula_packaging = '${pkgFormula}' `
        }
        if (extFormula) {
            additional = additional + `,formula_extruder = '${extFormula}' `
        }
        if (printFormula) {
            additional = additional + `,formula_printing = '${printFormula}' `
        }

        let wip = ``
        if (mixer !== null) {
            wip = wip + ` ,is_wip_mixer = '${mixer}' `
        }
        if (extruder !== null) {
            wip = wip + ` ,is_wip_extruder = '${extruder}' `
        }
        if (printing !== null) {
            wip = wip + ` ,is_wip_printing = '${printing}' `
        }
        if (laminating !== null) {
            wip = wip + ` ,is_wip_laminating = '${laminating}' `
        }

        let upd = `update m_product set m_formula_id = '${mxFormula}' ${additional} ${wip}
        where m_product_id = '${m_product_id}'`

        console.log(`********* ${upd} *************`);

        await request.query(upd)
        res.status(200).json({ msg: `Berhasil update formula` });
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
}
const findOne = async (req, res) => {
    try {
        const { formula_id } = req.body
        const request = DB.promise();

        let sel1 = `select 
        a.formula_id,a.kode_formula,a.nama_formula,date_format(start_date,'%d %M, %y') as mulai
        ,date_format(end_date,'%d %M, %y') as sampai,a.keterangan
        ,case when isactive = 1 then 'Ya' else 'Tidak' end as aktivasi,
        b.kode_item,b.nama_item,
        round(b.qty,2) as qty_adukan,b.amount,
        date_format(start_date,'%Y-%m-%d') as start_date
        ,date_format(end_date,'%Y-%m-%d') as end_date
        ,isactive
        from formula a
        inner join formula_detail b on a.formula_id = b.formula_id
        where a.formula_id =  '${formula_id}' and b.qty > 0
        order by isactive,nama_formula`

        console.log(sel1);
        let [res1] = await request.query(sel1)
        res.status(200).json(res1);
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
}
const Create = async (req, res) => {
    try {
        const { startDate, endDate, kodeFormula, keterangan, namaFormula, detail } = req.body
        const newUUID = uuidv4();
        const request = DB.promise();
        let jml = 0
        for (let i = 0; i < detail.length; i++) {
            jml = jml + parseFloat(detail[i].qt_act)

            console.log(detail);
        }

        let insert1 = `INSERT INTO formula
        (formula_id, kode_formula, nama_formula, start_date, end_date, createdate, isactive, seq, keterangan, satuan, qty_lot)
        VALUES('${newUUID}', '${kodeFormula}', '${namaFormula}','${startDate}'
        ,'${endDate}', now(), 1, 1, '${keterangan}', 'Adukan', ${jml})`
        let [hasil] = await request.query(insert1)
        if (hasil) {
            for (let i = 0; i < detail.length; i++) {
                let cariItem = `select * from m_item 
                where kode_item = '${detail[i].kode_item}' 
                and nama_item = '${detail[i].nama_item}' and isactive = 1`

                let [itm] = await request.query(cariItem)

                let inz = `INSERT INTO formula_detail
                (formula_detail_id, formula_id, kode_formula, kode_item, nama_item, satuan, qty, amount)
                VALUES(uuid(), '${newUUID}', '${kodeFormula}', '${detail[i].kode_item}'
                ,'${detail[i].nama_item}', '${itm[0].uom}', ${detail[i].qt_act}, NULL)`

                await request.query(inz)
            }
        }
        console.log('ddd');
        res.status(200).json({ msg: "Data formula berhasil" });
        // res.status(500).json({ msg: '3333' });
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
}
const EditFormula = async (req, res) => {
    try {
        const { formula_id, edited, added, isactive } = req.body
        const request = DB.promise();
        const newUUID = uuidv4();

        console.log(isactive);
        if (isactive == 0) {
            let upf = `update formula set isactive = 0 where formula_id = '${formula_id}'`
            await request.query(upf)
            res.status(200).json({ msg: `Berhasil update formula` });
            return
        }

        let de = `select count(*)+1 as jml,nama_formula,keterangan from formula where formula_id = '${formula_id}'
        group by nama_formula`
        let [dt1] = await request.query(de)

        let section = dt1[0].keterangan
        console.log(section);
        // return
        let objEdit = ``
        let where = ``
        if (section == "EXTRUDER") {
            objEdit = ` formula_extruder = '${newUUID}' `
            where = ` and formula_extruder = '${formula_id}' `
        } else if (section == "MIXER") {
            objEdit = ` formula_id = '${newUUID}' `
            where = ` and formula_id = '${formula_id}' `
        } else if (section == "PRINTING") {
            objEdit = ` formula_printing = '${newUUID}' `
            where = ` and formula_printing = '${formula_id}' `
        } else if (section == "PACKING") {
            objEdit = ` formula_packaging = '${newUUID}' `
            where = ` and formula_packaging = '${formula_id}' `
        }
        let updOldProd = `update m_product set ${objEdit}
        where 1=1 ${where} `

        console.log(updOldProd);
        // return
        // duplicate dulu
        let buatH = `insert into formula
        select '${newUUID}',kode_formula,nama_formula,null,null,now(),1,'${dt1[0].jml}',keterangan,satuan,qty_lot,warna
        from formula
        where formula_id = '${formula_id}'`
        await request.query(buatH)

        let buatD = `insert into formula_detail 
        select uuid(),'${newUUID}',kode_formula,kode_item 
        ,nama_item ,satuan ,qty ,amount 
        from formula_detail fd where formula_id = '${formula_id}' and qty > 0`
        console.log(buatD, `***************************************`);
        await request.query(buatD)


        for (let i = 0; i < edited.length; i++) {
            // console.log(edited[i]);
            let upd = `update formula_detail set qty = '${edited[i].qt_act}' 
            where formula_id = '${newUUID}'
            and nama_item = '${edited[i].nama_item}'`
            await request.query(upd)
        }

        for (let o = 0; o < added.length; o++) {
            // console.log(added[o]);
            let pdr = `select * from m_item where nama_item = '${added[o].nama_item}'`
            let [prd] = await request.query(pdr)
            let satuan = prd[0].uom

            let insx = `insert into formula_detail
            select uuid(),'${newUUID}','${dt1[0].nama_formula}','${added[o].kode_item}'
            ,'${added[o].nama_item}','${satuan}','${added[o].qty}',null`
            // console.log(insx);
            await request.query(insx)

        }

        // update yang lama,
        let updOld = `update formula set isactive = 0 where formula_id = '${formula_id}' `
        await request.query(updOld)


        await request.query(updOldProd)

        res.status(200).json({ msg: `Berhasil update formula` });
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
}

module.exports = { find, updateProductFormula, EditFormula, Create, findOne }