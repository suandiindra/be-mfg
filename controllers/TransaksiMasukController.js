const mysql = require("mysql2/promise");
const { v4: uuidv4 } = require('uuid');
const { DB } = require('../config/conf.js');


const find = async (req, res) => {
    try {
        const request = DB.promise();
        let sel = `select a.transaksi_masuk_id,date_format(createddate,'%Y-%m-%d') as periode,nomor_po,nomor_sj,nama_partner 
        from transaksi_masuk a
        inner join transaksi_masuk_detail b on a.transaksi_masuk_id = b.transaksi_masuk_id
        inner join m_partner c on c.m_partner_id = a.m_partner_id
        group by a.transaksi_masuk_id,
        date_format(a.createdate,'%Y-%m-%d'),nomor_po,nomor_sj,nama_partner order by  createddate desc`
        let [res1] = await request.query(sel)
        res.status(200).json(res1);
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
}

const findOne = async (req, res) => {
    try {
        const { transaksi_masuk_id } = req.query
        const request = DB.promise();
        let sel = `select a.transaksi_masuk_id,date_format(createddate,'%Y-%m-%d') as periode,nomor_po,nomor_sj,nama_partner 
        ,b.* 
        from transaksi_masuk a
        inner join transaksi_masuk_detail b on a.transaksi_masuk_id = b.transaksi_masuk_id
        inner join m_partner c on c.m_partner_id = a.m_partner_id
        where a.transaksi_masuk_id = '${transaksi_masuk_id}'`

        let [res1] = await request.query(sel)
        res.status(200).json(res1);
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
}

const create = async (req, res) => {
    try {
        const { m_partner_id, createdate, m_user_id, nomor_po, nomor_sj, detail } = req.body
        const request = DB.promise();
        const newUUID = uuidv4();

        let ins1 = `INSERT INTO transaksi_masuk
        (transaksi_masuk_id, createdate, m_partner_id, nomor_po, nomor_sj, createdby)
        VALUES('${newUUID}','${createdate}', '${m_partner_id}', '${nomor_po}', '${nomor_sj}', '${m_user_id}')`

        let hsl = await request.query(ins1)
        for (const obj of detail) {
            let m_item_id = obj.m_item_id
            let nama_item = obj.nama_item
            let satuan = obj.uom
            let qty = obj.qty
            let price = obj.price

            let insd = `INSERT INTO transaksi_masuk_detail
            (transaksi_masuk_detail_id, transaksi_masuk_id, m_item_id, nama_item, satuan
            , qty, price, createddate)
            VALUES(uuid(), '${newUUID}', '${m_item_id}', '${nama_item}', '${satuan}'
            ,'${qty}', '${price}', now())`

            // update stok

            let upd = `update stok_rm set stok_masuk = stok_masuk + ${qty}, 
            stok_akhir = stok_akhir +  ${qty},
            lastupdate = now()
            where m_item_id = '${m_item_id}'
            and date_format(period,'%Y-%m-%d') = date_format(now(),'%Y-%m-%d')`



            let updItem = `update m_item set qty = qty + ${parseFloat(qty)},updatedAt = now()  
            where m_item_id = '${m_item_id}'`

            console.log(updItem);
            // return
            await request.query(insd)
            await request.query(upd)
            await request.query(updItem)
        }
        res.status(200).json({ msg: `Berhasil Masuk` });

    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
}

module.exports = { find, create, findOne };
