const express = require ('express');
// const bcrypt = require('bcrypt');
const saltRounds = 10;

const {DB} = require('../config/conf')

module.exports = {
    edituser : async function(req,res){
        try {
            const {m_user_id,pwd,isactive} = req.body 
            const request = DB.promise();
            let edit = ``
            if(pwd){
                edit = edit + ` ,password = '${await hashPassword(pwd)}'`
            }
            if(isactive){
                edit = edit + ` ,isactive = ${isactive} `
            }
            let upd = `update m_user set m_user_id = '${m_user_id}' ${edit} where m_user_id = '${m_user_id}'`

            console.log(upd);
            let op = await request.query(upd)
            if(op){
                res.status(200).json({msg:`berhasil`});
            }
        } catch (error) {
            console.log(error);
            res.status(500).json(error);
        }
    },
    find : async function(req,res){
        try {
            const request = DB.promise();
            let sel = `select m_user_id,nik,nama_user,email
            ,case when role = 1 then 'Administrator' 
            when role = 2 then 'PPIC'
            when role = 3 then 'Admin' end as roles,isactive
            from m_user mu `
            let data = await request.query(sel)
            data = data[0]
            
            res.status(200).json({data});
        } catch (error) {
            console.log(error);
            res.status(500).json(error);
        }
    },
    createUser: async function(req,res){
        try {
            const {nik,nama,email,password,role} = req.body
            const request = DB.promise();
            let sel = `INSERT INTO m_user
            (m_user_id, nik, nama_user, email, password, role, createdAt, updatedAt,isactive)
            VALUES(uuid(), '${nik}', '${nama}', '${email}', '${await hashPassword(password)}', '${role}', now(), now(),1)`
            
            await request.query(sel)
            res.status(200).json({msg:`Berhasil`});
        } catch (error) {
            console.log(error);
            res.status(500).json(error);
        }
    },
    resetPwd: async function(req,res){
        try {
            const request = DB.promise();
            const {m_user_id} = req.body
            let upd = `update m_user set password = '${await hashPassword('123')}' where m_user_id = '${m_user_id}' `
            await request.query(upd)
            res.status(200).json({msg:`Berhasil`});
        } catch (error) {
            console.log(error);
            res.status(500).json(error);
        }

    }
}
async function hashPassword(password) {
    const request = DB.promise();
    let cek = `select SHA2('${password}', 256) as encrypPwd`
    let [dp] = await request.query(cek)
    return dp[0].encrypPwd
  }