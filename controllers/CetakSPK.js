const path = require("path");
const fs = require("fs");
const { urlGlobal } = require("../config/conf.js")

module.exports = {
    setTemplatePenawaran: async function (header, section, adukan) {
        console.log('****', header.img_fg);
        // <img src="${urlGlobal}product/logoImg" style="width:70px;heigh:70px;position: absolute; top: 40px; left: 40px;"/>
        let imgLogo = await convertImageToBase64(`./assets/interindo.png`)
        let imgFG = header.img_fg ? await convertImageToBase64(`./assets/FG/${header.img_fg}`) : null
        // let imgFG = null
        let extd = header.line_extruder
        let html = `<!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Form Survey</title>
            <style>
                p{
                    font-size: 9px;
                }
                .border {
                    display: flex;
                    align-items: center;
                    height: 90px;
                    width: 100%;
                    border-radius: 5px;
                }
                .btm{
                    margin-bottom: -12px;
                    font-family: Tahoma, sans-serif;
                    font-size: 14px;
                }
                .btm2{
                    margin-bottom: -12px;
                    font-family: Tahoma, sans-serif;
                    font-size: 16px;
                }
                .header {
                    font-family: Tahoma, sans-serif;
                    font-weight: bold;
                    font-size: 24px;
                    margin-left: 20px;
                    color: antiquewhite;
                    width: 60%;
                    padding-top: 3px;
                }
                .headerRight {
                    font-family: Tahoma, sans-serif;
                    font-weight: bold;
                    font-size: 24px;
                    margin-left: 20px;
                    width: 40%;
                    padding-top: 3px;
                }
                .infoheader {
                    display: flex;
                    padding: 20px;
                    font-family: Tahoma, sans-serif;
                }
                .infoheader-left {
                    width: 10%;
                    float: left;
                    padding-right: 10px;
                    background-color:'green'
                }
                .kiri {
                    width: 50%;
                    float: left;
                    padding-right: 10px;
                    background-color:'green'
                }
                .kanan {
                    width: 50%;
                    float: right;
                    background-color:'red'
                }
                .infoheader-right {
                    width: 90%;
                    float: right;
                    background-color:'red'
                }
                .infoheader p {
                    font-size: 16px;
                }
                table {
                    width: 100%;
                    border-collapse: collapse;
                    margin-top: 20px;
                    font-family: Tahoma, sans-serif;
                    font-size: 6px;
                }
                th, td {
                    padding: 10px;
                    text-align: left;
                }
                th {
                    background-color: #f2f2f2; /* Warna latar belakang header */
                }
                tr:nth-child(even) {
                    background-color: #f2f2f2; /* Warna latar belakang bergantian tiap baris genap */
                }
            </style>
        </head>
        <body style="padding:30px">
            <div class="border">
                <div class="header">
                    <img src="data:image/png;base64,${imgLogo}" alt="Logo" style="width:80px;heigh:80px;position: absolute; top: 40px; left: 40px;" />
                </div>
            </div>
            <div style="padding:15px">
                <div class="infoheader-left">
                    <img src="data:image/png;base64,${imgFG}" alt="No Image Product" style="width:130px;heigh:130px;position: absolute; top: 140px; left: 55px;" />
                </div>
                <div class="infoheader-right">
                    <p style="font-weight: bold;font-family: Tahoma, sans-serif;text-align: center;margin-right:20px;margin-top:-100px">WORK IN PROGRESS ${section}</p>
                    <p style="font-weight: bold;font-family: Tahoma, sans-serif;text-align: center;margin-right:20px;margin-top:0px">${header.nomor_spk}</p>
                </div>
            </div>
            <hr style="margin-top:-75px;margin-bottom:-40px"/>
            <div style="width: 100%; background-color:'red'">
                <div style="width: 30%; height: 100px; float: left"> 
                    &nbsp
                </div>
                <div style="width: 70%; height: 100px; float: right">
                    ${sideKonten(header, adukan, section, extd)}
                </div>
            </div>
            <div style="margin-top:290px">
                ${cekView(section, extd)}
            </div>
            ${footer(header, section)}
        </body>
        </html>`
        return html;
    },
    setTesting: function () {
        let html = `<!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Form Survey</title>
            <style>
                p{
                    font-size: 9px;
                }
                .border {
                    display: flex;
                    align-items: center;
                    height: 90px;
                    width: 100%;
                    border-radius: 5px;
                }
                .btm{
                    margin-bottom: -12px;
                    font-family: Tahoma, sans-serif;
                    font-size: 14px;
                }
                .btm2{
                    margin-bottom: -12px;
                    font-family: Tahoma, sans-serif;
                    font-size: 16px;
                }
                .header {
                    font-family: Tahoma, sans-serif;
                    font-weight: bold;
                    font-size: 24px;
                    margin-left: 20px;
                    color: antiquewhite;
                    width: 60%;
                    padding-top: 3px;
                }
                .headerRight {
                    font-family: Tahoma, sans-serif;
                    font-weight: bold;
                    font-size: 24px;
                    margin-left: 20px;
                    width: 40%;
                    padding-top: 3px;
                }
                .infoheader {
                    display: flex;
                    padding: 20px;
                    font-family: Tahoma, sans-serif;
                }
                .infoheader-left {
                    width: 10%;
                    float: left;
                    padding-right: 10px;
                    background-color:'green'
                }
                .kiri {
                    width: 50%;
                    float: left;
                    padding-right: 10px;
                    background-color:'green'
                }
                .kanan {
                    width: 50%;
                    float: right;
                    background-color:'red'
                }
                .infoheader-right {
                    width: 90%;
                    float: right;
                    background-color:'red'
                }
                .infoheader p {
                    font-size: 16px;
                }
                table {
                    width: 100%;
                    border-collapse: collapse;
                    margin-top: 20px;
                    font-family: Tahoma, sans-serif;
                    font-size: 6px;
                }
                th, td {
                    padding: 10px;
                    text-align: left;
                }
                th {
                    background-color: #f2f2f2; /* Warna latar belakang header */
                }
                tr:nth-child(even) {
                    background-color: #f2f2f2; /* Warna latar belakang bergantian tiap baris genap */
                }
            </style>
        </head>
        <body style="padding:30px">
        </body>`
        return html;
    }
}
async function convertImageToBase64(img) {
    try {
        // const response = await axios.get(url, { responseType: 'arraybuffer' });
        // const base64Data = Buffer.from(response.data, 'binary').toString('base64');
        const imageBuffer = fs.readFileSync(img);
        const base64Data = imageBuffer.toString('base64');
        return base64Data;
    } catch (error) {
        console.error('Error fetching image:', error.message);
        throw error;
    }
}
function mixerResult(extd) {
    let html = `
        <table border="1">
            <tr>
                <td>Shift</td>
                <td>Tanggal</td>
                <td>Jam Mulai</td>
                <td>Jam Selesai</td>
                <td>Co.Aditif</td>
                <td>Koordinator</td>
                <td>Qty Adukan</td>
                <td>Sisa Adukan (Kg)</td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </table>
    `
    return html
}
function cekView(v, extd) {
    if (v == "MIXING") {
        return mixerResult(extd)
    } else if (v == "EXTRUDER") {
        return extruderResult()
    } else if (v == "PRINTING") {
        return printingResult()
    } else if (v == "PACKING") {
        return packingResult()
    } else if (v == "LAMINATING") {
        return laminatingResult()
    }
}
function cekLine(v, obj) {
    if (v == "MIXING") {
        return obj.line_mixing
    } else if (v == "EXTRUDER") {
        return obj.line_extruder
    } else if (v == "PRINTING") {
        return obj.line_printing
    } else if (v == "LAMINATING") {
        return obj.line_laminating
    } else {
        return ''
    }
}
function extruderResult() {
    let html = `
        <table border="1">
            <tr>
                <td>Shift</td>
                <td>Tanggal</td>
                <td>Jam Mulai</td>
                <td>Jam Selesai</td>
                <td>Operator</td>
                <td>QC/Helper</td>
                <td>Qty Lembar</td>
                <td>Hsl Sementar</td>
                <td>Reject</td>
                <td>Berat/m1</td>
                <td>Cycle Time</td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        `
    return html
}
function printingResult() {
    let html = `
        <table border="1">
            <tr>
                <td colspan="5" style="text-align:center">Hasil Produksi Printing</td>
                <td colspan="1" style="text-align:center">Extruder</td>
                <td colspan="1" style="text-align:center">Printing</td>
                <td colspan="2" style="text-align:center">Reject</td>
            </tr>
            <tr>
                <td>Shift</td>
                <td>Tanggal</td>
                <td>Jam</td>
                <td>Operator</td>
                <td>Koordinator</td>
                <td>Qty Lembar</td>
                <td>Qty Dus</td>
                <td>Ext Lbr</td>
                <td>Print Lbr</td>
            </tr>
            <tr>
                <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
            </tr>
            <tr>
                <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
            </tr>
            <tr>
                <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
            </tr>
            <tr>
                <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
            </tr>
            <tr>
                <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
            </tr>
            <tr>
                <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
            </tr>
            <tr>
                <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
            </tr>
            <tr>
                <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
            </tr>
            <tr>
                <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
            </tr>
            <tr>
                <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
            </tr>
        </table>`
    return html
}
function packingResult() {
    let html = `
        <table border="1">
            <tr>
                <td>Tanggal</td>
                <td>Jam</td>
                <td>Operator</td>
                <td>Koordinator</td>
                <td>Qty</td>
            </tr>
            <tr>
                <td></td><td></td><td></td><td></td><td></td>
            </tr>
            <tr>
                <td></td><td></td><td></td><td></td><td></td>
            </tr>
            <tr>
                <td></td><td></td><td></td><td></td><td></td>
            </tr>
            <tr>
                <td></td><td></td><td></td><td></td><td></td>
            </tr>
            <tr>
                <td></td><td></td><td></td><td></td><td></td>
            </tr>
            <tr>
                <td></td><td></td><td></td><td></td><td></td>
            </tr>
            <tr>
                <td></td><td></td><td></td><td></td><td></td>
            </tr>
            <tr>
                <td></td><td></td><td></td><td></td><td></td>
            </tr>
            <tr>
                <td></td><td></td><td></td><td></td><td></td>        
            </tr>
            <tr>
                <td></td><td></td><td></td><td></td><td></td>
            </tr>
        </table>`
    return html
}
function laminatingResult() {
    let html = `
        <table border="1">
            <tr>
                <td>Tanggal</td>
                <td>Jam</td>
                <td>Operator</td>
                <td>Koordinator</td>
                <td>Qty</td>
            </tr>
            <tr>
                <td></td><td></td><td></td><td></td><td></td>
            </tr>
            <tr>
                <td></td><td></td><td></td><td></td><td></td>
            </tr>
            <tr>
                <td></td><td></td><td></td><td></td><td></td>
            </tr>
            <tr>
                <td></td><td></td><td></td><td></td><td></td>
            </tr>
            <tr>
                <td></td><td></td><td></td><td></td><td></td>
            </tr>
            <tr>
                <td></td><td></td><td></td><td></td><td></td>
            </tr>
            <tr>
                <td></td><td></td><td></td><td></td><td></td>
            </tr>
            <tr>
                <td></td><td></td><td></td><td></td><td></td>
            </tr>
            <tr>
                <td></td><td></td><td></td><td></td><td></td>        
            </tr>
            <tr>
                <td></td><td></td><td></td><td></td><td></td>
            </tr>
        </table>`
    return html
}
function footer(isi, v) {
    let catatan = ``
    if (v == 'MIXING') {
        catatan = isi.note_mixer
    } else if (v == 'EXTRUDER') {
        catatan = isi.note_extruder
    } else if (v == 'PRINTING') {
        catatan = isi.note_printing
    } else if (v == 'LAMINATING') {
        catatan = isi.note_laminating
    } else if (v == 'PACKING') {
        catatan = isi.note_packing

    }

    // catatan = `catatan oke`
    let notes = ``
    try {
        notes = catatan.split(';');
    } catch (error) {
        notes = ``
    }
    let html = `
        <div style="width: 100%;">
            <Table border="1">
                <tr style="height:0px">
                    <td style="width:'50%'">
                        ${notes.length > 0 ? notes[0] : ''}
                    </td>
                    <td style="width:'50%'">
                        ${notes.length > 0 ? notes[1] ? notes[1] : '' : ''}
                    </td>
                </tr>
            </table>
            <div>
                <p>Dibuat Oleh : </p>
            </div>
        </div>
    `

    return html
}
function sideKonten(header, adukan, section, extd) {
    let html = `<div style="width: 50%; height: 100px; float: left;margin-top:-40px">
        <b style="font-size:7px">Informasi SPK</b>
        <table>
            <tr>
                <td>Sales Order</td>
                <td>:</td>
                <td>${header.nomor_po}</td>
            </tr>
            <tr>
                <td>Customer</td>
                <td>:</td>
                <td>${header.nama_partner}</td>
            </tr>
            <tr>
                <td>Kode Index</td>
                <td>:</td>
                <td>${header.tipe}</td>
            </tr>
            <tr>
                <td>Qty Produksi (lbr)</td>
                <td>:</td>
                <td>${header.qty}</td>
            </tr>
            <tr>
                <td>Qty Dus</td>
                <td>:</td>
                <td>${header.qty_kemasan}</td>
            </tr>
            <tr>
                <td>Qty/Dus (Pcs)</td>
                <td>:</td>
                <td>${parseFloat(header.qty) / parseFloat(header.qty_kemasan)}</td>
            </tr>
        </table>
    </div> 
    <div style="width: 50%; height: 100px; float: right;margin-top:-40px">
        <b style="font-size:7px">Informasi Produk</b>
        <table>
            <tr>
                <td>Product</td>
                <td>:</td>
                <td>${header.nama_product}</td>
            </tr>
            <tr>
                <td>Panjang (cm)</td>
                <td>:</td>
                <td>${header.panjang}</td>
            </tr>
            <tr>
                <td>Lebar (cm)</td>
                <td>:</td>
                <td>${header.lebar}</td>
            </tr>
            <tr>
                <td>Tebal (mm)</td>
                <td>:</td>
                <td>${header.tebal}</td>
            </tr>
            <tr>
                <td>Warna Papan</td>
                <td>:</td>
                <td>${header.warna_mixing}</td>
            </tr>
            <tr>
                <td>Line Mesin</td>
                <td>:</td>
                <td>${cekLine(section, header)}</td>
            </tr>
            ${section == 'EXTRUDER' ? `
            <tr>
                <td>List</td>
                <td>:</td>
                <td>${header.listnat}</td>
            </tr>`: ``}
            
            ${section == 'MIXING' ? `<tr>
                <td>Line Extruder</td>
                <td>:</td>
                <td>${extd}</td>
            </tr>` : ``}
            ${adukan ? `<tr>
            <td>Jumlah Adukan</td>
            <td>:</td>
            <td>${adukan}</td>
        </tr>` : ""}
        </table>
    </div> `
    return html
}
