const express = require('express');
const uuidv4 = require('uuid');
const { DB } = require('../config/conf.js');
// const bcrypt = require('bcrypt');
const saltRounds = 10;

// const argon2 = require('argon2')
const comparePassword = async (inputPassword, hashedPassword) => {
    // const match = await bcrypt.compare(inputPassword, hashedPassword);
    // return match;
    const request = DB.promise();
    let cek = `select SHA2('${inputPassword}', 256) as encrypPwd`
    let [dp] = await request.query(cek)
    let pwd = dp[0].encrypPwd
    if (hashedPassword == pwd) {
        return true
    }
    return false
}
const Login = async (req, res) => {
    try {
        const request = DB.promise();
        let selx = `select * from m_user where email = '${req.body.email}' and isactive = 1`

        console.log(selx);
        let [user] = await request.query(selx)
        if (user.length == 0) {
            console.log('gak nemu');
            return res.status(500).json({ msg: `Tidak ditemukan` });
        }
        user = user[0]
        const match = await comparePassword(req.body.password, user.password);
        if (!match) {
            console.log(`lahh... salah`);
            return res.status(400).json({ msg: "Wrong Password" });
        }
        const uuid = user.m_user_id;
        const name = user.nama_user;
        const email = user.email;
        const role = user.role;
        // tambahan
        const [rows] = await request.execute(`select * from role_menu where user_role = ${role}`);
        // console.log(`select * from role_menu where user_role = ${role}`);
        let objMenu = []
        for (let i = 0; i < rows.length; i++) {
            let header = rows[i].menu_header
            let obj = {}
            obj.label = rows[i].menu_header
            if (rows[i].child) {
                let submenu = rows[i].child

                let qr = `select * from m_menu where menu_header = '${header}'
                and has_child in (${submenu}) order by has_child asc`
                let [sub] = await request.execute(qr);
                // console.log(sub);
                let menuSub = []
                for (let o = 0; o < sub.length; o++) {
                    // console.log(sub[o]);
                    let objx = {
                        label: sub[o].menu_child,
                        key: sub[o].direction
                    }
                    menuSub.push(objx)
                }
                obj.key = `#`
                obj.children = menuSub
            } else {
                let qr = `select * from m_menu where menu_header = '${header}' order by has_child asc`
                let [sub] = await request.execute(qr);
                obj.key = sub[0].direction
            }
            objMenu.push(obj)
        }
        let menu = objMenu
        // console.log(menu);

        return res.status(200).json({ uuid, name, email, role, menu });

    } catch (error) {
        console.log(error);
        return res.status(500).json({ msg: error });

    }
}
const Me = async (req, res) => {
    const { m_user_id } = req.query
    try {
        console.log('woiiii');
        const request = DB.promise();
        let sel = `select * from m_user where m_user_id = '${m_user_id}'`
        let op = await request.query(sel)
        if (op[0].length == 0) {
            return res.status(500).json({ msg: `gagal` });
        }

        // cek 
        const [rows] = await request.execute(`select * from role_menu`);
        let objMenu = []
        for (let i = 0; i < rows.length; i++) {
            let header = rows[i].menu_header
            let obj = {}
            obj.label = rows[i].menu_header
            if (rows[i].child) {
                let submenu = rows[i].child

                let qr = `select * from m_menu where menu_header = '${header}'
            and has_child in (${submenu}) order by has_child asc`
                let [sub] = await request.execute(qr);
                // console.log(sub);
                let menuSub = []
                for (let o = 0; o < sub.length; o++) {
                    // console.log(sub[o]);
                    let objx = {
                        label: sub[o].menu_child,
                        key: sub[o].direction
                    }
                    menuSub.push(objx)
                }
                obj.key = `#`
                obj.children = menuSub
            } else {
                let qr = `select * from m_menu where menu_header = '${header}' order by has_child asc`
                let [sub] = await request.execute(qr);
                obj.key = sub[0].direction
            }
            objMenu.push(obj)
        }
        let menu = objMenu
        console.log(menu);

        return res.status(200).json({ menu });
    } catch (error) {
        console.log(error);
        return res.status(500).json({ msg: error })
    }
    // if(!req.session.userId){
    //     return res.status(401).json({msg: "Mohon login ke akun Anda!"});
    // }
    // // let user = []
    // let user = await User.findOne({
    //     attributes:['m_user_id','nik','nama_user','email','role'],
    //     where: {
    //         m_user_id: req.session.userId
    //     }
    // });
    // if(!user) return res.status(404).json({msg: "User tidak ditemukan"});

    // const request = await mysql.createConnection(dbConfig);
    // // const [objMenu] = await request.execute(`select * from m_menu`);

    // const [rows] = await request.execute(`select * from role_menu`);
    // let objMenu = []
    // for(let i = 0; i<rows.length;i++){
    //     let header = rows[i].menu_header
    //     let obj = {}
    //     obj.label = rows[i].menu_header
    //     if(rows[i].child){
    //         let submenu = rows[i].child

    //         let qr = `select * from m_menu where menu_header = '${header}'
    //         and has_child in (${submenu}) order by has_child asc`
    //         let [sub] = await request.execute(qr);
    //         // console.log(sub);
    //         let menuSub = []
    //         for (let o = 0;o<sub.length;o++){
    //             // console.log(sub[o]);
    //             let objx = {
    //                 label : sub[o].menu_child,
    //                 key : sub[o].direction
    //             }
    //             menuSub.push(objx)
    //         }
    //         obj.key = `#`
    //         obj.children = menuSub
    //     }else{
    //         let qr = `select * from m_menu where menu_header = '${header}' order by has_child asc`
    //         let [sub] = await request.execute(qr);
    //         obj.key = sub[0].direction
    //         // obj.children = []
    //     }
    //     objMenu.push(obj)
    // }
    // console.log(objMenu);
    // const objek = {
    //     user : user,
    //     menu : objMenu
    // }
    // res.status(200).json(objek);
}
module.exports = { Login, Me }