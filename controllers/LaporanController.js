const express = require('express');
const uuidv4 = require('uuid');
const { DB } = require('../config/conf.js');

const hasilSpk = async (req,res) =>{
    try {
        const request = DB.promise();

        // res.status(200).json(res1);
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
}
const salesOrder = async (req,res) =>{
    try {
        const request = DB.promise();

        // res.status(200).json(res1);
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
}
const reject = async (req,res) =>{
    try {
        const {tgl1,tgl2} =  req.body
        const request = DB.promise();
        let qr = `select sections,shift,date_format(periode ,'%d, %M %y') as periode ,s.line_extruder,sum(reject) as reject 
        ,s.nama_product,s.warna_mixing ,sr.operator ,sr.koordinator 
        from spk_result sr 
        inner join spk s on s.spk_id = sr.spk_id 
        where reject is not null`
        let [res1] = await request.query(qr)
        res.status(200).json(res1);
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
}

module.exports = {reject,salesOrder,hasilSpk}
