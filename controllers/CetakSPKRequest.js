const path = require("path");
const fs = require("fs");
const {urlGlobal} = require("../config/conf.js")

module.exports = {
    // <img src="${urlGlobal}product/logoImg" style="width:170px;heigh:170px"/>
    setTemplatePermintaan: function (header,sec) {
        // console.log('****', header);
        let html = `<!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Form Survey</title>
            <style>
                p{
                    font-size: 9px;
                }
                b{
                    font-size: 9px;
                }
                .border {
                    display: flex;
                    align-items: center;
                    height: 90px;
                    width: 100%;
                    border-radius: 5px;
                }
                .btm{
                    margin-bottom: -12px;
                    font-family: Tahoma, sans-serif;
                    font-size: 14px;
                }
                .btm2{
                    margin-bottom: -12px;
                    font-family: Tahoma, sans-serif;
                    font-size: 16px;
                }
                .header {
                    font-family: Tahoma, sans-serif;
                    font-weight: bold;
                    font-size: 24px;
                    margin-left: 20px;
                    color: antiquewhite;
                    width: 60%;
                    padding-top: 3px;
                }
                .headerRight {
                    font-family: Tahoma, sans-serif;
                    font-weight: bold;
                    font-size: 24px;
                    margin-left: 20px;
                    width: 40%;
                    padding-top: 3px;
                }
                .infoheader {
                    display: flex;
                    padding: 20px;
                    font-family: Tahoma, sans-serif;
                }
                .infoheader-left {
                    width: 10%;
                    float: left;
                    padding-right: 10px;
                    background-color:'green'
                }
                .kiri {
                    width: 50%;
                    float: left;
                    padding-right: 10px;
                    background-color:'green'
                }
                .kanan {
                    width: 50%;
                    float: right;
                    background-color:'red'
                }
                .infoheader-right {
                    width: 90%;
                    float: right;
                    background-color:'red'
                }
                .infoheader p {
                    font-size: 16px;
                }
                table {
                    width: 100%;
                    border-collapse: collapse;
                    margin-top: 20px;
                    font-family: Tahoma, sans-serif;
                    font-size: 6px;
                }
                th, td {
                    padding: 10px;
                    text-align: left;
                }
                th {
                    background-color: #f2f2f2; /* Warna latar belakang header */
                }
                tr:nth-child(even) {
                    background-color: #f2f2f2; /* Warna latar belakang bergantian tiap baris genap */
                }
            </style>
        </head>
        <body style="padding:30px">
            <div class="border">
                <div class="header">
                    
                </div>
            </div>
            <div style="padding:15px,margin-top:-20px">
                <div class="infoheader-left">
                </div>
                <div class="infoheader-right">
                    <p style="font-weight: bold;font-family: Tahoma, sans-serif;text-align: center;margin-right:20px;margin-top:-100px">FORM PERMINTAAN RAW MATERIAL</p>
                    <p style="font-weight: bold;font-family: Tahoma, sans-serif;text-align: center;margin-right:20px;margin-top:0px">${header[0].nomor_spk}</p>
                </div>
            </div>
            <hr style="margin-top:-67px"/>
            ${tableMixer(header,sec)}
        </body>
        </html>`
        return html;

    }
}
function tableMixer(data,sec) {
    let arrMixer = data.filter((o) => {
        return o.sections == sec && o.qty > 0
    })
    let html = ``
    if (arrMixer.length > 0) {
        html = `
        <div style="margin-top:-10px;margin-bottom:-30px">
            <b>Section : ${sec}</b></br>
            <b>Line : ${data[0].line_mixing}</b>
        </div>
        <table border="1"style="margin-top:-100px">
            <tr>
                <td>No</td>
                <td>Kode Item</td>
                <td>Nama Item</td>
                <td>Satuan</td>
                <td>Qty</td>
                <td>Request Qty</td>
            </tr>
            ${isi(arrMixer)}
        </table>
        </br>
        <div style="float:left"><p><b>Requestor : </b></p></div>
        <div style="float:right"><p><b>Petugas : </b></p></div>
        
        `
    }
    return html
}

function isi(dt){
    let tbl = ``
    for(let i = 0; i<dt.length;i++){
        tbl = tbl + `<tr>
        <td>${i+1}</td>
        <td>${dt[i].kode_item}</td>
        <td>${dt[i].nama_item}</td>
        <td>${dt[i].satuan}</td>
        <td>${dt[i].qty.toFixed(2)}</td>
        <td></td>
    </tr>`
    }
    return tbl
}