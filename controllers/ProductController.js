// import mProductModel from "../models/mProductModel.js";
// import User from "../models/UserModel.js";
// import {Op} from "sequelize";
const path = require("path");
const mysql = require("mysql2/promise");
const uuidv4 = require('uuid');
const { DB } = require('../config/conf.js');


const getFormulaExtruder = async (req, res) => {
    try {
        const { m_product_id, barang } = req.query
        const request = DB.promise();


        let key_id = ``
        if (m_product_id) {
            key_id = m_product_id
        } else {
            let sel2 = `select * from m_product where nama_barang = '${barang}'`
            let [dt] = await request.query(sel2)
            key_id = dt[0].m_product_id
        }
        let sel = `select * from extruder_item where m_product_id  = '${key_id}' `
        console.log(sel);
        let [response] = await request.query(sel)
        console.log(response, '***');
        res.status(200).json(response);

    } catch (error) {
        res.status(500).json({ msg: error.message });

    }
}
const actionMolding = async function (req, res) {
    try {
        const { ob } = req.body
        const request = DB.promise();
        let upd = ``
        if (ob.moldingId) {
            upd = `update m_tipe_molding set isactive = 1, target = '${ob.target}' 
            , mesin = '${ob.mesin}' 
            where m_tipe_molding_id = '${ob.moldingId}'`
            await request.query(upd)
        } else {
            upd = `insert into m_tipe_molding select uuid(),'${ob.tipeMolding}',1,'${ob.target}',now(),'${ob.mesin}'`
            await request.query(upd)
        }
        console.log(upd);
        res.status(200).json({ msg: `Berhasil` });
    } catch (error) {
        res.status(500).json({ msg: error.message });
    }
}
const getMolding = async (req, res) => {
    try {
        const request = DB.promise();
        let sel = `select * from m_tipe_molding where isactive = 1`
        let [dr] = await request.query(sel)
        console.log(dr);
        res.status(200).json(dr);
    } catch (error) {
        res.status(500).json({ msg: error.message });

    }
}
const getProducts = async (req, res) => {
    try {
        const request = DB.promise();
        let sel = `select * from m_product mp where isactive = 1 order by updatedAt desc`
        let [response] = await request.query(sel)
        res.status(200).json(response);
    } catch (error) {
        res.status(500).json({ msg: error.message });
    }
}
// const getProductById = async(req, res) =>{
//     try {
//         const request = DB.promise();

//         const product = await Product.findOne({
//             where:{
//                 uuid: req.params.id
//             }
//         });
//         if(!product) return res.status(404).json({msg: "Data tidak ditemukan"});
//         let response;
//         if(req.role === "admin"){
//             response = await Product.findOne({
//                 attributes:['uuid','name','price'],
//                 where:{
//                     id: product.id
//                 },
//                 include:[{
//                     model: User,
//                     attributes:['name','email']
//                 }]
//             });
//         }else{
//             response = await Product.findOne({
//                 attributes:['uuid','name','price'],
//                 where:{
//                     [Op.and]:[{id: product.id}, {userId: req.userId}]
//                 },
//                 include:[{
//                     model: User,
//                     attributes:['name','email']
//                 }]
//             });
//         }
//         res.status(200).json(response);
//     } catch (error) {
//         res.status(500).json({msg: error.message});
//     }
// }
const getImage = (req, res) => {
    try {
        const __dirname = path.resolve();
        const assetsDirectory = path.join(
            __dirname,
            `assets/FG/${req.params.file}`
        );
        console.log(assetsDirectory);
        const imagePath = path.join(assetsDirectory);
        res.sendFile(imagePath);
        // res.status(500).json({msg: `pdo`});
    } catch (error) {
        console.log(error);
    }
}
const logoImg = (req, res) => {
    try {
        const __dirname = path.resolve();
        const assetsDirectory = path.join(
            __dirname,
            `assets/interindo.png`
        );
        // console.log(assetsDirectory);
        const imagePath = path.join(assetsDirectory);
        res.sendFile(imagePath);
        // res.status(500).json({msg: `pdo`});
    } catch (error) {
        console.log(error);
    }
}
const updateImage = async (req, res) => {
    try {
        const { m_product_id } = req.body
        let dokumen1 = ``;
        if (req.file) {
            dokumen1 = req.file.originalname;
        }
        const request = DB.promise();
        let upx = `update m_product set img = '${dokumen1}' where m_product_id = '${m_product_id}'`
        await request.query(upx)
        res.status(200).json({ msg: "Product Updated Successfuly" });
    } catch (error) {
        res.status(500).json({ msg: error.message });
    }
}
const createProduct = async (req, res) => {
    const { kode_barang, nama_barang, kategori, jenis_barang, satuan, tebal, supplier
        , brand, price, panjang, work_center, formula_extruder, formula_printing
        , deffect, formula_laminating, lebar
        , formula_packaging, moldingPilih } = req.body
    const request = DB.promise();

    console.log('aaa');
    let dokumen1 = ``;
    if (req.file) {
        dokumen1 = req.file.originalname;
    }
    // console.log(dokumen1, work_center, moldingPilih, '**********');
    // return
    // res.status(200).json({msg: "Product Created Successfuly"});
    try {
        let masuk = `INSERT INTO m_product
        (m_product_id, kode_barang, nama_barang, kategori, jenis_barang
        , satuan, tebal, panjang, supplier, brand, price, price_cogs
        , img, work_center, m_formula_id, isactive, createdAt, updatedAt, formula_extruder
        , formula_printing, formula_laminating, formula_packaging,defect,lebar,tipe_molding)
        VALUES(uuid(), '${kode_barang}', '${nama_barang}', '${kategori}', '${jenis_barang}'
        , '${satuan}', '${tebal}', '${panjang}', NULL, '${brand}', ${price}, NULL
        , '${dokumen1}', '${work_center}', NULL, 1, now(), now(), NULL, NULL, NULL, NULL,'${deffect}','${lebar}','${moldingPilih}')`

        console.log(masuk);
        await request.query(masuk)

        res.status(200).json({ msg: "Product Created Successfuly" });
    } catch (error) {
        res.status(500).json({ msg: error.message });
    }
}
const updateImg = async (req, res) => {
    try {
        const request = DB.promise();
        const { m_product_id } = req.body

        let dokumen1 = ``;
        if (req.file) {
            dokumen1 = req.file.originalname;
        }

    } catch (error) {

    }
}
const updateProduct = async (req, res) => {
    try {
        const { m_product_id, kode_barang, nama_barang, kategori, jenisBarang, satuan, tebal, supplier
            , brand, price, panjang, isactive, work_center, addItem, lebar, moldingPilih } = req.body;
        const request = DB.promise();

        // console.log(req.body.work_center.split(','));
        let forDt = req.body.work_center.split(',')
        let mix = null
        let ext = null
        let print = null
        let lam = null
        let pack = null
        for (let i = 1; i <= 5; i++) {
            let dx = forDt.find((o) => {
                return o == i
            })
            if (!dx) {
                if (i == 1) {
                    mix = ` m_formula_id = null `
                } else if (i == 2) {
                    ext = ` formula_extruder = null `
                } else if (i == 3) {
                    print = ` formula_printing = null `
                } else if (i == 4) {
                    lam = ` formula_laminating = null `
                } else if (i == 5) {
                    pack = ` formula_packaging = null `
                }
            }
        }
        let updExt = `delete from extruder_item where m_product_id = '${m_product_id}'`
        let union1 = null
        let union2 = null
        let union3 = null

        if (isactive == 0) {
            let upd = `update m_product set isactive = 0 where m_product_id = '${m_product_id}'`
            await request.query(upd)
            res.status(200).json({ msg: "Product updated successfuly" });
        } else {
            if (addItem) {
                if (addItem.hotstamp && addItem.hotstampqty > 0) {
                    union1 = `select uuid(),'${m_product_id}','${addItem.hotstamp}','${addItem.hotstamp}','m1','${addItem.hotstampqty}',0,now(),'HOTSTAMP'`
                }

                if (addItem.nat && addItem.qtynat > 0) {
                    let add = ``
                    if (union1) {
                        add = ` union `
                    }
                    union2 = ` ${add} select uuid(),'${m_product_id}','${addItem.nat}','${addItem.nat}','m1','${addItem.qtynat}',0,now(),'NAT'`
                }

                if (addItem.laminate && addItem.laminateqty > 0) {
                    let add = ``
                    if (union1 || union2) {
                        add = ` union `
                    }
                    union3 = ` ${add} select uuid(),'${m_product_id}','${addItem.laminate}','${addItem.laminate}','m1','${addItem.laminateqty}',0,now(),'LAMINATE'`
                }
                // console.log(union1, union2, union3);
                await request.query(updExt)
                if (!union1 && !union2 && !union3) {

                } else {
                    let masuk = `insert into extruder_item 
                ${union1 ? union1 : ''}${union2 ? union2 : ''}${union3 ? union3 : ''}`

                    console.log(masuk);
                    // return
                    await request.query(masuk)
                }
            }
            // return



            let updateQuery = "UPDATE m_product SET ";
            let updateColumns = [];

            if (kode_barang) updateColumns.push(`kode_barang = '${kode_barang}'`);
            if (lebar) updateColumns.push(`lebar = '${lebar}'`);
            if (nama_barang) updateColumns.push(`nama_barang = '${nama_barang}'`);
            if (kategori) updateColumns.push(`kategori = '${kategori}'`);
            if (jenisBarang) updateColumns.push(`jenis_barang = '${jenisBarang}'`);
            if (satuan) updateColumns.push(`satuan = '${satuan}'`);
            if (tebal) updateColumns.push(`tebal = '${tebal}'`);
            if (supplier) updateColumns.push(`supplier = '${supplier}'`);
            if (brand) updateColumns.push(`brand = '${brand}'`);
            if (price) updateColumns.push(`price = '${price}'`);
            if (panjang) updateColumns.push(`panjang = '${panjang}'`);
            if (isactive !== undefined) updateColumns.push(`isactive = ${isactive ? 1 : 0}`);
            if (work_center) updateColumns.push(`work_center = '${work_center}'`);
            if (moldingPilih) updateColumns.push(`tipe_molding = '${moldingPilih}'`);

            if (mix) updateColumns.push(mix);
            if (ext) updateColumns.push(ext);
            if (print) updateColumns.push(print);
            if (lam) updateColumns.push(lam);
            if (pack) updateColumns.push(pack);

            updateQuery += updateColumns.join(", ") + ` WHERE m_product_id = '${m_product_id}'`;

            console.log(updateQuery);
            // return

            let rt = await request.query(updateQuery)
            res.status(200).json({ msg: "Product updated successfuly" });
        }

    } catch (error) {
        console.log(error);
        res.status(500).json({ msg: error.message });
    }
}

// const deleteProduct = async(req, res) =>{
//     try {
//         const product = await Product.findOne({
//             where:{
//                 uuid: req.params.id
//             }
//         });
//         if(!product) return res.status(404).json({msg: "Data tidak ditemukan"});
//         const {name, price} = req.body;
//         if(req.role === "admin"){
//             await Product.destroy({
//                 where:{
//                     id: product.id
//                 }
//             });
//         }else{
//             if(req.userId !== product.userId) return res.status(403).json({msg: "Akses terlarang"});
//             await Product.destroy({
//                 where:{
//                     [Op.and]:[{id: product.id}, {userId: req.userId}]
//                 }
//             });
//         }
//         res.status(200).json({msg: "Product deleted successfuly"});
//     } catch (error) {
//         res.status(500).json({msg: error.message});
//     }
// }

module.exports = {
    getProducts, createProduct, updateProduct, actionMolding
    , getImage, logoImg, getFormulaExtruder, updateImage, getMolding
}