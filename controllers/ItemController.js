const express = require('express')
const { DB } = require('../config/conf.js');
const { v4: uuidv4 } = require('uuid');

const create = async (req, res) => {
    const { kode_item, nama_item, price, qty, tipe, uom } = req.body;
    try {
        const request = DB.promise();
        const unik = uuidv4()
        const insertQuery = `
            INSERT INTO m_item (m_item_id,kode_item, nama_item, uom, qty, price, tipe, isactive)
            VALUES ('${unik}','${kode_item}', '${nama_item}', '${uom}', ${qty}, ${price}, '${tipe}', 1)
        `;
        // console.log(insertQuery);
        // insert ke stok 
        let masuk = `INSERT INTO stok_rm
        (stok_rm_id, period, m_item_id, kode_item, nama_item, stok_awal, stok_masuk, stok_keluar, stok_adjust, stok_akhir, lastupdate)
        VALUES(uuid(), now(),'${unik}', '${kode_item}', '${nama_item}', 0, 0, 0, 0, 0, now());
        `
        await request.query(insertQuery)
        await request.query(masuk)
        return res.status(200).json({ msg: 'Item Created Successfully' });
    } catch (error) {
        console.log(error);
        res.status(500).json({ msg: error.message });
    }
};
const getItemExtruder = async (req, res) => {
    try {
        const request = DB.promise();
        let sel = `select * from m_item mi where isactive = 1 and tipe in ('NAT','LAMINATE','HOTSTAMP')
        order by tipe `
        hasil = await request.query(sel)
        res.status(200).json(hasil[0]);

    } catch (error) {
        res.status(500).json({ msg: error.message });
    }
}
const getTipe = async (req, res) => {
    try {
        const request = DB.promise();
        let sel = `select * from m_tipe mp order by nama_tipe asc`
        let response = await request.query(sel)
        res.status(200).json(response[0]);
    } catch (error) {
        res.status(500).json({ msg: error.message });
    }
}
const getItem = async (req, res) => {
    const { m_item_id, tipe } = req.query;
    console.log(req.query);
    try {
        const request = DB.promise();
        let response;
        console.log(m_item_id);
        let qrsel = ``
        if (m_item_id) {
            qrsel = `select * from m_item where isactive = 1 and m_item_id = '${m_item_id}'`
        } else {
            qrsel = `select * from m_item where isactive = 1`
        }
        if (tipe) {
            qrsel = `select * from m_item where isactive = 1 and tipe = '${tipe}'`
        }
        console.log(qrsel);
        response = await request.query(qrsel)
        res.status(200).json(response[0]);
    } catch (error) {
        res.status(500).json({ msg: error.message });
    }
}
const updateItem = async (req, res) => {
    try {
        const { kode_item, m_item_id, nama_item, uom, qty, price, tipe, isactive } = req.body;
        const request = DB.promise();

        const updateQuery = `UPDATE m_item
        SET kode_item = '${kode_item}',
            nama_item = '${nama_item}',
            uom = '${uom}',
            qty = ${qty},
            price = ${price},
            tipe = '${tipe}',
            isactive = ${isactive}
        WHERE m_item_id = '${m_item_id}'`;

        console.log(updateQuery);
        // return
        await request.query(updateQuery)
        res.status(200).json({ msg: `Berhasil` });

    } catch (error) {
        console.log(error);
        res.status(500).json({ msg: error.message });
    }
}
const deleteItem = async (req, res) => {
    try {
        const { m_item_id } = req.body;
        const request = DB.promise();

        let dels = `update m_item set isactive = 0 where m_item_id = '${m_item_id}'`
        await request.query(dels)
        return res.status(200).json({ msg: `Berhasil dihapus` });
    } catch (error) {
        return res.status(500).json({ msg: error.message });
    }
}
const getUOM = async (req, res) => {
    try {
        const request = DB.promise();
        let sel = `select * from m_satuan ms `
        let response = await request.query(sel)
        res.status(200).json(response[0]);
    } catch (error) {
        res.status(500).json({ msg: error.message });
    }
}
const keluarItem = async (req, res) => {
    try {
        const { header, detail } = req.body
        const request = DB.promise();
        const myUuid = uuidv4();

        console.log(detail);

        let insertH = `INSERT INTO transaksi_keluar
        (transaksi_keluar_id, createdate, nomor_dok, nomor_spk, shift, line
        , requestor, isClosed, isactive,sections)
        VALUES('${myUuid}', now(), NULL, '${header.nomorSpk}', '${header.shift}', null
        , '${header.nama}', 1, 1,'${detail[0].sections}')`

        // console.log(insertH);
        let rt = await request.query(insertH)
        if (rt) {
            for (let i = 0; i < detail.length; i++) {
                let yi = `select * from m_item where kode_item = '${detail[i].kode_item}'`
                let u = await request.query(yi)
                let obj = u[0][0]
                if (obj) {
                    let inseD = `INSERT INTO transaksi_keluar_detail
                    (transaksi_keluar_detail_id, transaksi_keluar_id, m_item_id, nama_barang
                    , satuan, qty, createdate,spk_detail_formula_id)
                    VALUES(uuid(), '${myUuid}', '${obj.m_item_id}','${detail[i].nama_item}'
                    , '${detail[i].satuan}', '${detail[i].request}', now(),'${detail[i].spk_detail_formula_id}');`

                    await request.query(inseD)

                    let upd = `update stok_rm set stok_keluar = stok_keluar + ${detail[i].request} 
                    where m_item_id = '${obj.m_item_id}'
                    and date_format(period ,'%Y-%m') = date_format(now() ,'%Y-%m') `
                    await request.query(upd)
                }
            }
        }
        res.status(200).json({ msg: 'Berhasil' });
    } catch (error) {
        res.status(500).json({ msg: error.message });
    }
}
const findKeluar = async (req, res) => {
    try {
        const request = DB.promise();
        let sel = `select b.nomor_spk,b.spk_id,tk.transaksi_keluar_id 
        ,date_format(createddate,'%Y-%m-%d') as period ,b.nama_product 
        ,tk.sections,tk.shift ,requestor 
        from transaksi_keluar tk 
        inner join spk b on b.nomor_spk = tk.nomor_spk
        where tk.isactive = 1 
        order by tk.createdate desc`
        // console.log(sel);
        let response = await request.query(sel)
        res.status(200).json(response[0]);
    } catch (error) {
        res.status(500).json({ msg: error.message });
    }
}
const findOneKeluar = async (req, res) => {
    try {
        const { id } = req.query
        const request = DB.promise();
        let sel = `select tkd.*,b.kode_item 
        from transaksi_keluar_detail tkd 
        inner join m_item b on b.m_item_id = tkd.m_item_id 
        where transaksi_keluar_id = '${id}'`
        console.log(sel);
        let response = await request.query(sel)
        res.status(200).json(response[0]);
    } catch (error) {
        res.status(500).json({ msg: error.message });
    }
}
module.exports = {
    getUOM, deleteItem, getItem
    , keluarItem, create, updateItem
    , findKeluar, findOneKeluar, getItemExtruder, getTipe
}