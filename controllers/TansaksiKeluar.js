const mysql = require("mysql2/promise");
const { v4: uuidv4 } = require('uuid');
const { DB } = require('../config/conf.js');
const find = async function (req, res) {
    try {
        const request = DB.promise();
        let sel = `select * from transaksi_keluar 
        where isactive = 1 order by createdate desc `
        let [res1] = await request.query(sel)
        res.status(200).json(res1);

    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
}
const transksiKeluarFG = async function (req, res) {
    const { objek } = req.body
    try {
        console.log(objek.length);

        const request = DB.promise();
        const unik = uuidv4()
        let insh = `insert into t_sales (t_sales_id,nomor_po,customer,createdate,isactive,createdby)
        values ('${unik}','${objek[0].nomor_po}','${objek[0].nama_partner}',now(),1,'xxx')`

        // console.log(insh);
        await request.query(insh)
        for (let i = 0; i < objek.length; i++) {

            if (objek[i].qty_so > 0) {
                let insd = `INSERT INTO t_sales_detail
                (t_sales_detail_id, t_sales_id, nama_item, satuan, qty, isactive, createdate)
                VALUES(uuid(), '${unik}', '${objek[i].nama_product}', 'lbr', ${objek[i].qty_so}
                , 1, now());`

                console.log(insd);
                await request.query(insd)
            }
        }

        res.status(200).json({ msg: `Berhasil` });

    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
}
const findSalesOrder = async function (req, res) {
    try {
        const { tgl1, tgl2, cust, nomorso, key } = req.body
        const request = DB.promise();
        let filter = ``

        console.log(req.body);
        if (tgl1) {
            // filter = ` and date_format(crt,'%Y-%m-%d') between '${tgl1}' and '${tgl2}' `
        }
        if (cust) {
            filter += ` and c.nama_partner = '${cust}' `
        }
        if (nomorso) {
            filter += ` and s.nomor_po = '${nomorso}' `
        }
        // let sel = `select distinct s.nomor_po,s.nomor_spk,s.nama_product,c.nama_partner
        // ,qty_produksi,panjang,lebar ,s.warna_mixing
        // ,s.qty_kemasan,b.qty as qty_result
        // ,b.dus
        // ,concat(floor((b.qty / qty_produksi) * 100),' %') as persen
        // ,qty_produksi - c.qty as sisa
        // ,c.qty as qty_keluar,date_format(crt,'%Y-%m-%d') as crt
        // from spk s
        // inner join m_partner c on c.m_partner_id = s.partner_id
        // inner join (select spk_id,sum(qty) as qty,sum(qty_dus) as dus from spk_result
        //     where sections = 'PACKING'
        //     and isactive = 1
        //     group by spk_id
        // )b on b.spk_id = s.spk_id
        // left join (
        //     select a.t_sales_id,nomor_po,customer,b.nama_item,sum(qty) as qty 
        //     ,max(a.createdate) as crt
        //     from t_sales a
        //     inner join t_sales_detail b on a.t_sales_id = b.t_sales_id
        //     where a.isactive = 1 and b.isactive = 1
        //     group by a.t_sales_id,nomor_po,customer,b.nama_item
        // )c on c.nomor_po = s.nomor_po and c.nama_item = s.nama_product 
        // where s.isactive = 1 ${filter}
        // order by crt desc `

        let sel = `select s.nomor_po,s.nomor_spk,s.nama_product,c.nama_partner
        ,qty_produksi,panjang,lebar ,s.warna_mixing
        ,s.qty_kemasan,b.qty as qty_result
        ,b.dus
        ,concat(floor((b.qty / qty_produksi) * 100),' %') as persen
        ,qty_produksi - sum(c.qty) as sisa
        ,sum(c.qty) as qty_keluar
        from spk s
        inner join m_partner c on c.m_partner_id = s.partner_id
        inner join (select spk_id,sum(qty) as qty,sum(qty_dus) as dus from spk_result
            where sections = 'PACKING'
            and isactive = 1
            group by spk_id
        )b on b.spk_id = s.spk_id
        left join (
            select a.t_sales_id,nomor_po,customer,b.nama_item,sum(qty) as qty
            ,max(a.createdate) as crt
            from t_sales a
            inner join t_sales_detail b on a.t_sales_id = b.t_sales_id
            where a.isactive = 1 and b.isactive = 1
            group by a.t_sales_id,nomor_po,customer,b.nama_item
        )c on c.nomor_po = s.nomor_po and c.nama_item = s.nama_product
        where s.isactive = 1 ${filter}
        group by s.nomor_po,s.nomor_spk,s.nama_product,c.nama_partner
        ,qty_produksi,panjang,lebar ,s.warna_mixing
        ,s.qty_kemasan,b.qty,qty_produksi
        ,b.dus
        order by crt desc`
        console.log(sel);
        let dt = await request.query(sel)
        res.status(200).json(dt[0]);
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
}
const findPO = async function (req, res) {
    const { nomor_po } = req.body
    try {
        const request = DB.promise();

        let sel = `select distinct s.nomor_po,s.nomor_spk,s.nama_product,c.nama_partner
        ,qty_produksi,s.panjang,s.lebar ,s.warna_mixing
        ,s.qty_kemasan,b.qty as qty_result
        ,b.dus,x.jenis_barang 
        ,concat(floor((b.qty / qty_produksi) * 100),' %') as persen
        ,c.qty as qty_keluar
        from spk s
        inner join m_partner c on c.m_partner_id = s.partner_id
        inner join (select spk_id,sum(qty) as qty,sum(qty_dus) as dus from spk_result
            where sections = 'PACKING'
            and isactive = 1
            group by spk_id
        )b on b.spk_id = s.spk_id
        left join (
            select nomor_po,customer,b.nama_item,sum(qty) as qty from t_sales a
            inner join t_sales_detail b on a.t_sales_id = b.t_sales_id
            where a.isactive = 1 and b.isactive = 1
            group by nomor_po,customer,b.nama_item
        )c on c.nomor_po = s.nomor_po and c.nama_item = s.nama_product 
        inner join m_product x on x.nama_barang = s.nama_product and x.isactive = 1
        where s.isactive = 1 and s.nomor_po = '${nomor_po}'`

        // console.log(sel);
        let dt = await request.query(sel)
        res.status(200).json(dt[0]);

    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
}
module.exports = { find, findPO, transksiKeluarFG, findSalesOrder };
