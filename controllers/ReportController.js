const express = require('express')
const { DB } = require('../config/conf.js');

const spkResult = async (req, res) => {
    try {
        const { section, tgl1, tgl2 } = req.body
        const request = DB.promise();
        let qr = ``
        if (section == "EXTRUDER") {
            let filter = ` and sr.sections = '${section}' `
            if (tgl1) {
                filter = filter + ` and date_format(periode ,'%Y-%m-%d') between '${tgl1}' and '${tgl2}' `
            }
            qr = `select sr.spk_result_id,b.nomor_po as nomor_so,b.nomor_spk,b.nama_product,sr.sections,date_format(periode ,'%d, %M %y')as tgl 
            ,jam_mulai ,jam_selesai ,operator,rpm,cycletime ,qty,reject ,b.line_extruder 
            from spk_result sr 
            inner join spk b on b.spk_id = sr.spk_id 
            where sr.isactive = 1 ${filter} order by sr.createdate desc`
        } else if (section == "MIXER") {
            console.log(`***************88`);
            let filter = ` and sr.sections = '${section}' `
            if (tgl1) {
                filter = filter + ` and date_format(periode ,'%Y-%m-%d') between '${tgl1}' and '${tgl2}' `
            }
            qr = `select sr.spk_result_id,b.nomor_po as nomor_so, b.nomor_spk,b.nama_product,sr.sections,date_format(periode ,'%d, %M %y')as tgl 
            ,jam_mulai ,jam_selesai,sr.shift,aditif ,adukan ,sisa ,b.line_mixing ,warna_mixing
            from spk_result sr 
            inner join spk b on b.spk_id = sr.spk_id 
            where sr.isactive = 1 ${filter} order by sr.createdate desc`
        } else if (section == "PRINTING") {
            let filter = ` and sr.sections = '${section}' `
            if (tgl1) {
                filter = filter + ` and date_format(periode ,'%Y-%m-%d') between '${tgl1}' and '${tgl2}' `
            }
            qr = `select sr.spk_result_id,b.nomor_po as nomor_so, b.nomor_spk,b.nama_product,sr.sections
            ,date_format(periode ,'%d-%m-%y')as tgl 
            ,jam_mulai,sr.shift,sr.operator ,sr.koordinator ,sr.qty ,sr.reject 
            ,b.line_printing 
            from spk_result sr 
            inner join spk b on b.spk_id = sr.spk_id 
            where sr.isactive = 1 ${filter} order by sr.createdate desc`
        } else if (section == "PACKING") {
            let filter = ` and sr.sections = '${section}' `
            if (tgl1) {
                filter = filter + ` and date_format(periode ,'%Y-%m-%d') between '${tgl1}' and '${tgl2}' `
            }

            qr = `select sr.spk_result_id,b.nomor_po as nomor_so, b.nomor_spk,b.nama_product,sr.sections,date_format(periode ,'%d, %M %y')as tgl 
            ,jam_mulai,sr.operator ,sr.koordinator ,sr.qty 
            ,b.line_packaging,sr.qty_dus
            from spk_result sr 
            inner join spk b on b.spk_id = sr.spk_id 
            where sr.isactive = 1 ${filter} order by sr.createdate desc`
        } else if (section == "LAMINATING") {
            let filter = ` and sr.sections = '${section}' `
            if (tgl1) {
                filter = filter + ` and date_format(periode ,'%Y-%m-%d') between '${tgl1}' and '${tgl2}' `
            }

            qr = `select sr.spk_result_id,b.nomor_po as nomor_so, b.nomor_spk,b.nama_product
            ,sr.sections,date_format(periode ,'%d, %M %y')as tgl 
            ,jam_mulai,sr.operator ,sr.koordinator ,sr.qty 
            ,b.line_laminating,sr.jam_selesai,sr.reject 
            from spk_result sr 
            inner join spk b on b.spk_id = sr.spk_id 
            where sr.isactive = 1 ${filter} order by sr.createdate desc`
        }
        console.log(section, qr);
        let [result] = await request.query(qr)
        return res.status(200).json(result);
    } catch (error) {
        console.log(error);
        res.status(500).json({ msg: error });
    }
}

const salesOrder = async (req, res) => {
    try {
        const { status, customer_id, tgl1, tgl2, keys } = req.body

        console.log(req.body);
        const request = DB.promise();
        let filter = ``
        if (status) {
            filter = filter + ` and status_spk = '${status}' `
        }
        if (customer_id) {
            let xc = `select * from m_partner mp where m_partner_id = '${customer_id}'`
            let [dg] = await request.query(xc)
            if (dg.length > 0) {
                filter = filter + ` and s.partner_id = '${customer_id}' `
            }
        }

        if (tgl1 && tgl2) {
            filter = filter + ` and date_format(createddate,'%Y-%m-%d') between '${tgl1}' and '${tgl2}' `
        }

        if (keys) {
            filter = filter + ` and s.nomor_po like '${keys}' or s.nomor_spk like '%${keys}%' or s.nama_product like '%${keys}%'`
        }
        let sel = `select s.nomor_po,s.nomor_spk,s.nama_product,c.nama_partner
        ,qty_produksi,panjang,lebar ,s.warna_mixing
        ,s.qty_kemasan,b.qty as qty_result
        ,b.dus
        ,concat(floor((b.qty / qty_produksi) * 100),' %') as persen
        from spk s
        inner join m_partner c on c.m_partner_id = s.partner_id
        left join (select spk_id,sum(qty) as qty,sum(qty_dus) as dus from spk_result
            where sections = 'PACKING'
            and isactive = 1
            group by spk_id
        )b on b.spk_id = s.spk_id
        where s.isactive = 1 ${filter}
        group by s.nomor_po,s.nomor_spk,s.nama_product,c.nama_partner
        ,qty_produksi,panjang,lebar ,s.warna_mixing
        ,s.qty_kemasan,b.dus
        order by s.createddate,s.nomor_po desc`

        console.log(sel);

        let [result] = await request.query(sel)
        return res.status(200).json(result);
    } catch (error) {
        console.log(error);
        res.status(500).json({ msg: error });
    }
}
const mutasi = async (req, res) => {
    try {
        const { status } = req.body
        const request = DB.promise();

        let sel = `select mi.m_item_id,mi.kode_item,mi.nama_item ,tipe 
        ,coalesce(b.stok_awal,0) as stok_awal
        ,coalesce(b.stok_masuk ,0) as stok_masuk
        ,coalesce(b.stok_keluar ,0) as stok_keluar
        ,coalesce(b.stok_akhir ,0) as stok_akhir
        from m_item mi 
        left join stok_rm b on b.m_item_id = mi.m_item_id 
        and date_format(period ,'%Y-%m') = date_format(now() ,'%Y-%m') `

        let [hsl] = await request.query(sel)
        return res.status(200).json(hsl);
    } catch (error) {
        console.log(error);
        res.status(500).json({ msg: error });
    }
}

const reject = async (req, res) => {
    try {
        const { section, tgl1, tgl2 } = req.body
        const request = DB.promise();
        let filter = ``

        console.log(req.body);
        if (section !== "All") {
            console.log(section);
            filter = ` and sections = '${section}' `
        }
        if (tgl1) {
            filter = ` and date_format(periode,'%Y-%m-%d') between '${tgl1}' and '${tgl2}' `
        }
        // let qr = `select sections,shift,date_format(periode ,'%d, %M %y') as periode ,s.line_extruder,sum(reject) as reject 
        // ,s.nama_product,s.warna_mixing ,sr.operator ,sr.koordinator 
        // from spk_result sr 
        // inner join spk s on s.spk_id = sr.spk_id 
        // where reject is not null ${filter}`

        let qr = `select s.nomor_spk ,sections,shift,date_format(periode ,'%d, %M %y') as periode 
        ,s.nama_product,s.warna_mixing ,sr.operator ,sr.koordinator,sr.persen
        ,sr.reject ,sr.persen,sr.qty 
        ,case when sections = 'MIXER' then s.line_mixing 
        when sections = 'EXTRUDER' then s.line_extruder 
        when sections = 'PRINTING' then s.line_printing 
        when sections = 'PACKING' then s.line_packaging end as line_mesin
        from spk_result sr 
        inner join spk s on s.spk_id = sr.spk_id
        where s.isactive = 1 and sr.reject is not null ${filter}`

        console.log(qr);
        let [res1] = await request.query(qr)
        res.status(200).json(res1);
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
}
module.exports = {
    spkResult, salesOrder, mutasi, reject
}