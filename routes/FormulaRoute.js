const express = require("express");
const {find
    ,findOne,Create,updateProductFormula,EditFormula
} = require('../controllers/FormulaController.js')
const router = express.Router();

router.get('/find',find);
router.post('/findOne',findOne);
router.post('/Create',Create);
router.post('/updateProductFormula',updateProductFormula);
router.post('/EditFormula',EditFormula);


module.exports = router;
