const express = require("express");
const {spkResult,salesOrder,mutasi,reject} = require(`../controllers/ReportController`)
const router = express.Router();


router.post('/spkResult',spkResult);
router.post('/salesOrder',salesOrder);
router.post('/mutasi',mutasi);
router.post('/reject',reject);

module.exports = router
