const express = require("express");
const {find,create,update,reset} = require('../controllers/PartnerController.js')

const router = express.Router();

router.get('/find',find);
router.post('/update',update);
router.post('/Create',create);
router.post('/reset',reset);

module.exports = router;

