const express = require ('express');
const {
    getUsers,
    getUserById,
    createUser,
    updateUser,
    deleteUser
} = require("../controllers/Users.js")
const { verifyUser, adminOnly,cekToken } = require("../middleware/AuthUser.js")

const router = express.Router();

router.get('/users',getUsers);
router.get('/users/:id', getUserById);
router.post('/createUser', createUser);
router.post('/updateUser/:id', updateUser);
router.post('/delUser/:id',deleteUser);

export default router;