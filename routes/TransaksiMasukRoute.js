const express = require("express");
const { create, find, findOne, } = require("../controllers/TransaksiMasukController.js")
const { findPO, transksiKeluarFG, findSalesOrder } = require("../controllers/TansaksiKeluar.js")
const router = express.Router();

router.post('/create', create);
router.get('/find', find);
router.get('/findOne', findOne);
router.post('/findPO', findPO);
router.post('/findSalesOrder', findSalesOrder);
router.post('/transksiKeluarFG', transksiKeluarFG);

module.exports = router
