const express = require('express');
const {getUOM,deleteItem,getItem,create,getTipe
    ,updateItem,keluarItem,findKeluar,getItemExtruder
    ,findOneKeluar} = require('../controllers/ItemController.js');
const {createKategori,delKategori,
    getKategori} = require('../controllers/KategoriController.js');

const router = express.Router();

router.get('/getUOM', getUOM);
router.get('/getItem', getItem);
router.post('/create', create);
router.post('/updateItem', updateItem);
router.post('/deleteItem', deleteItem);
router.post('/deleteItem', create); // Harap periksa ulang, tampaknya ada duplikasi endpoint
router.post('/createKategori', createKategori);
router.post('/keluarItem', keluarItem);
router.post('/delKategori', delKategori);
router.get('/getKategori', getKategori);
router.get('/findKeluar', findKeluar);
router.get('/getTipe', getTipe);
router.get('/findOneKeluar', findOneKeluar);
router.get('/getItemExtruder', getItemExtruder);

module.exports = router;
