const express = require("express");
const { getProdukFormula, createSPK, hasil, deleteResult, addnote, deleteSPK, testCetak
    , find, findOne, cetak, cetkPermintaan, findFormulaSec, testingPdf } = require("../controllers/SpkController.js")
const router = express.Router();

router.post('/getProdukFormula', getProdukFormula);
router.post('/createSPK', createSPK);
router.get('/find', find);
router.get('/findOne', findOne);
router.get('/cetak', cetak);
router.post('/hasil', hasil);
router.post('/addnote', addnote);
router.post('/deleteResult', deleteResult);
router.get('/cetkPermintaan', cetkPermintaan);
router.get('/findFormulaSec', findFormulaSec);
router.get('/testCetak', testCetak);
router.post('/deleteSPK', deleteSPK);
router.get('/testingPdf', testingPdf);

module.exports = router

