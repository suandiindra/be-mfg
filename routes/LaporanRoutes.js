const express = require("express");
const {hasilSpk,salesOrder,reject} = require('../controllers/LaporanController.js')

const router = express.Router();

router.get('/hasilSpk',hasilSpk);
router.post('/salesOrder',salesOrder);
router.post('/reject',reject);

module.exports = router;