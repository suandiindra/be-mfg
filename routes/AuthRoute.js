const express = require('express');
const { Login, Me
    // logOut, Me, daftarToken 
} = require('../controllers/AuthController.js');

const { createUser, find, edituser } = require('../controllers/userController.js')

const router = express.Router();

// router.post('/token', Me);
router.get('/find', find);
router.get('/me', Me);
router.post('/login', Login);
router.post('/edituser', edituser);
router.post('/createUser', createUser);

// router.post('/login',async (req, res) => {
//     console.log('ininnnnn');
//     res.header("Access-Control-Allow-Origin", "*");
//     res.header(
//       "Access-Control-Allow-Headers",
//       "Origin, X-Requested-With, Content-Type, Accept"
//     );
//     await Login(req, res);
//   });

// router.post('/daftarToken', daftarToken);
// router.delete('/logout', logOut);

module.exports = router;
