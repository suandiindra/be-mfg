const express = require("express");
const {
  getProducts,
  updateImage,
  createProduct,
  updateProduct,
  logoImg, actionMolding,
  getImage, getFormulaExtruder, getMolding
} = require("../controllers/ProductController.js");
// const { verifyUser } = require("../middleware/AuthUser.js");
const path = require('path');
const fs = require('fs');
const multer = require('multer');


const router = express.Router();
const diskStorage = multer.diskStorage({
  destination: function (req, file, cb) {
    var dir = `./assets/FG`;
    console.log(dir, "dir");
    console.log(file, "file");
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }
    cb(null, dir);
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname);
  },
});
const upload = multer({ storage: diskStorage });

router.get('/getProducts', getProducts);
router.get('/getFormulaExtruder', getFormulaExtruder);
router.get('/getMolding', getMolding);
router.post('/actionMolding', actionMolding);
// router.get('/',getProducts);
router.get('/getImage/:file', getImage);
router.get('/logoImg', logoImg);
// router.get('/products/:id',verifyUser, getProductById);
router.post('/createProduct', upload.single("img1"), createProduct);
router.post('/updateImage', upload.single("img1"), updateImage);
router.post('/updateProduct', updateProduct);
// router.delete('/products/:id',verifyUser, deleteProduct);

module.exports = router